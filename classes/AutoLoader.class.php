<?php
// Load my root class
require_once(__ROOT_DIR . '/classes/MyObject.class.php');

class AutoLoader extends MyObject {
	
	public function __construct() {
		spl_autoload_register(array($this, 'load'));
	}
	
	// This method will be automatically executed by PHP whenever it encounters
	// an unknown class name in the source code
	private function load($className) {
		$nom = ucfirst($className.'.class.php');
		$repertoire = array('classes','model','controller','view');//Liste des répertoires ou sont possiblement les éléments a importer.
		foreach($repertoire as $rep){
			if( is_readable(__ROOT_DIR .'/'.$rep.'/'.$nom) ) {
				require_once(__ROOT_DIR .'/'.$rep.'/'.$nom);
			}
		}
	}
}
$__LOADER = new AutoLoader();
?>
