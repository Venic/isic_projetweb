<?php
/**
 *
 */
class Dispatcher extends MyObject{

  public function __construct(){
  }


	public static function dispatch($request){
		$nameOfController = $request->getControllerName().'Controller';//On prend le nom du controller courant
		//echo $request->getControllerName();
		return new $nameOfController($request);//On retourne le bon controller
	}

 }
 ?>
