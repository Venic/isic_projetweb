<?php

/**
 *
 */
class DatabasePDO extends PDO
{
	protected $db_adress = 'localhost';//Mon adresse;
	protected $db_name = 'six_qui_ramasse';//Le nom de la base de donnee.
	protected $db_login = 'root';//Mon pseudo;
	protected $db_password = 'root'; //Mon mot de passe

	protected static $uniquebd = NULL;
	public $PDOInstance = null;

	public static function getUniqueDataBase(){
		if(is_null(static::$uniquebd)){
			try{
				static::$uniquebd = new PDO('mysql:host=localhost;dbname=six_qui_ramasse','root','root');
			}
			catch(Exception $e){
				die('Error while connecting to MySQL.<br>');
			}
		}
		return static::$uniquebd;
	}

	public function query($query){
		return $this->PDOInstance->query($query);
	}
}
 ?>
