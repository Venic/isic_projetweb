<?php

class Request extends MyObject
{
    public static $currentRequest = null;

	public function __construct(){}

    public static function getCurrentRequest(){
		//Singleton pour retourner la requête courante
		if (is_null(static::$currentRequest)) {
			static::$currentRequest = new static();
		}
		return static::$currentRequest;
    }

    public function getControllerName() {
		//Return le nom du controller demandé dans la requête
		if(isset($_GET['controller'])){
			return $_GET['controller'];
		}
		elseif(isset($_POST['controller'])){
			return $_POST['controller'];
		}
		elseif(isset($_COOKIE['controller'])){
			return $_COOKIE['controller'];
		}
		return 'Anonymous';
	}

    public function getActionName() {
		//Return le nom de l'action demandée dans la requête
		if(isset($_GET['action'])){
			return $_GET['action'];
		}
		elseif(isset($_POST['action'])){
			return $_POST['action'];
		}
		elseif(isset($_COOKIE['action'])){
			return $_COOKIE['action'];
		}
		return 'defaultAction';
	}

    public function read($string){
		//Renvoie la valeur de la requête correspondant au champ d'entrée. Renvoie '' si rien
		if (isset($_GET[$string])) {
			return $_GET[$string];
		}
		elseif (isset($_POST[$string])) {
			return $_POST[$string];
		}
		elseif(isset($_COOKIE[$string])){
			return $_COOKIE[$string];
		}else {
			return '';
		}
  }
	public function write($name, $value){
		//Ecrit dans la requête (en get)
		$_GET[$name]=$value;
	}

	public function write_post($name, $value){
		//Ecrit dans la requête (en post)
		$_POST[$name]=$value;
	}

	public function clear($target){
		//Efface le champ d'entrée de la requête
		unset($_GET[$target]);
		unset($_POST[$target]);
	}

	public function writeCookie($target,$value){
		//Création des cookies pour garder les session user en mémoire.
      if(!isset($_COOKIE[$target])){
        //TODO : repasser à 500 lorsqu'intervention terminée. 30 secondes autrement.
        setcookie($target, $value,time()+3600);
        $_COOKIE[$target]=$value;
      }
		}

	public static function clearCookie($target){
		//Detruit un cookie
		unset($_COOKIE[$target]);
		setcookie($target, null, time());
	}
}
 ?>
