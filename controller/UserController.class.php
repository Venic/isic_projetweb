<?php
/**
* List of all functions used in this Controller
* Name of the function          Type of element returned        Purpose of the function
* defaultAction                 void                            Sets all the information to be displayed on the user's homepage.
* setSound                      string                          Returns the location of the sound that should be played according to the hour.
* setGreetings                  string                          Returns the greetings that should be displayed according to the hour.
* validationInscription         void                            End of a former AnonymousController. Sets the right UserView if the user's subscription went well.
* validationConnexion           void                            End of a former AnonymousController. Sets the right UserView if the user's attempt to connect went well.
* disconnectUser                void                            Disconnects the User. Part of the whole process, end is in the AnonymousController.class.php file.
* createGame                    void                            Asks the createGame UserView (invites the User to give the name of his/her game) to be displayed.
* deleteGameSubscription        void                            Handles the process of removing our player from a game.
* launchGame                    void                            Does the tasks after the user wrote the name of his/her game.
* invitePlayers                 void                            Prepares the UserView that asks the user to invite another player.
* inviteAnotherUser             void                            Handles the process of adding or not another player to an existing game.
* buildingNewGame               void                            Prepares the board and hands at the beginning of each round.
* playWithCard                  void                            Handles the process of playing a card in a game.
* numberOfCardsOnRow            int                             Returns the number of cards in a row.
* checkingBoardContent          void                            Checks the content of board and allow or not the player to play.
* launchTheCalculus             void                            Handles the process of putting the cards at the right place on the board.
* emptyingARow                  void                            Updates players' score, empties the content of a row, puts the first card.
* getSmallestGapBetweenCards    int                             Returns the row index which has the lowest difference between its highest card and the card due to be played.
* getLeastPowerfulRow           int                             Returns the row index with the lowest amount of points. Called only if the player palys a very low card deliberately.
* smallerThanAllMaximas         boolean                         Determines if the card given is smaller than all the highest values on the board.
* buildingLaunchedGame          void                            Handles the UserView that is due to display the board. Called at the end of the process of calculus, setting up a new game...
* scoreGreaterThanSixtySix      boolean                         Trying to know if a player has a score higher than sixty-six. If true, process of ending the game will be triggered.
* createArrayofValues           array                           Genreates an array with values going from 1 to 105, with a step of 1.
* gameOver                      void                            Calls the right UserView at the end of a game.
* updatingGameStatus            void                            Handles the process of setting the players out of game and deleting the game is no one is no longer registered.
* passwordChangeDone            void                            Handles the process of changing the user's password.
* confirmDeletion               void                            Handles the process of deleting an account. Second part of the process is handled by an AnonymousController.
* changePassword                void                            Prepares the UserView that allows the password to be modified to be displayed.
* deleteAccount                 void                            Prepares the UserView that will allow the User to delete his account to be displayed.
* playerIsReady                 void                            Changes the state of a player when he says "I'm ready" while being in the waiting room.
* readRules                     void                            Prepares the UserView that is due to display the game rules.
* waitingRoom                   void                            Prepares the UserView of the waiting room after the player chose a game.
* classement                    void                            Prepares the table of rankings.
* generateErrorMessages         string                          Prepares the alert to be displayed on the score_moyen.
* generateButton                string                          Prepares the button on the list of available games.
* generateButtonReadRules       string                          Prepares the button to be displayed at the bottom of the readRules page.
*/
class UserController extends Controller
{

  function __construct($request){
    parent::__construct($request);
  }

  public function defaultAction($request){
    $greetings = $this->setGreetings($request);
    $errorMessage = '';
    $view = new UserView($this,'defaultAction');
    $view->setArg('pseudo',$request->read('pseudo'));
    $view->setArg('greetings',$greetings);
    if (strlen($request->read('message'))>0) {
      $errorMessage = $request->read('message');
    }
    $tableOfGames=0;
    if (!is_null(Player::getAllCreatedGamesInWhichPlayerIsInvolved($request->read('pseudo'))) && count(Player::getAllCreatedGamesInWhichPlayerIsInvolved($request->read('pseudo')))>0) {
      $tableOfGames = Player::getAllCreatedGamesInWhichPlayerIsInvolved($request->read('pseudo'));
      $keyString = 'gameInvolved';
    }
    else if (!is_null(Game::fetchAvailableGames()) && count(Game::fetchAvailableGames())>0) {
      $tableOfGames = Game::fetchAvailableGames();
      $keyString = 'gameList';
    } else{
      $errorMessage = $this->generateErrorMessages('no_games_available');
    }
    if ($tableOfGames!=0) {
      $beginWithGameAtRow = 0;
      $addThisNumberOfGames = 5;
      if (strlen($request->read('beginWith'))!=0) {
        $beginWithGameAtRow = $request->read('beginWith');
      }
      if ($beginWithGameAtRow+4 > count($tableOfGames)) {
        $addThisNumberOfGames = count($tableOfGames);
      }
      if ($beginWithGameAtRow+5< count($tableOfGames) && $beginWithGameAtRow-5>= 0) {
        $view->setArg('button',$this->generateButton($beginWithGameAtRow,'previous').' '.$this->generateButton($beginWithGameAtRow,'next'));
      } elseif ($beginWithGameAtRow+5< count($tableOfGames)) {
        $view->setArg('button',$this->generateButton($beginWithGameAtRow,'next'));
      } elseif ($beginWithGameAtRow-5>= 0) {
        $view->setArg('button',$this->generateButton($beginWithGameAtRow,'previous'));
      }
      $view->setArg($keyString,array_slice($tableOfGames,$beginWithGameAtRow,$addThisNumberOfGames));
      if ($beginWithGameAtRow==0) {
        $filePath = $this->setSound();
        $view->setArg('sound',$filePath);
      }
    }
    if (!is_null(User::getStats($request->read('pseudo')))){
      if (User::getStats($request->read('pseudo'))['played_games']==0) {
        $view->setArg('messageTwo',$this->generateErrorMessages('play_to_show_stats'));
      } else {
        $view->setArg('playerStats',User::getStats($request->read('pseudo')));
      }
    } else{
      $errorMessage = $errorMessage .'<br>'. $this->generateErrorMessages('error_in_stats');
    }
    $view->setArg('message',$errorMessage);
    $view->render();
  }

  public function setSound(){
    date_default_timezone_set("Europe/Paris");
    $today=getdate();
    $filePath='http://localhost/assets/';
    if ($today['hours']>=5 && $today['hours']<9 ) {
      $filePath=$filePath.'5-9.mp3';
    } else if ($today['hours']>=9 && $today['hours']<14) {
      $filePath=$filePath.'9-14.mp3';
    } else if ($today['hours']>=14 && $today['hours']<18) {
      $filePath=$filePath.'14-18.mp3';
    } else if ($today['hours']>=18 && $today['hours']<21) {
      $filePath=$filePath.'18-21.mp3';
    } else {
      $filePath=$filePath.'21-5.mp3';
    }
    return $filePath;
  }

  public function setGreetings($request){
    $string='';
    date_default_timezone_set("Europe/Paris");
    $today=getdate();
    if ($today['hours']>=0 && $today['hours']<6 ) {
      $string= 'Bonne nuit '.$request->read('pseudo').' !';
    } else if ($today['hours']>=6 && $today['hours']<12) {
      $string= 'Bonjour '.$request->read('pseudo').' !';
    } else if ($today['hours']>=12 && $today['hours']<18) {
      $string= 'Bon après-midi '.$request->read('pseudo').' !';
    } else {
      $string= 'Bonsoir '.$request->read('pseudo').' !';
    }
    return $string;
  }

  public function validationInscription($request){
    $view = new UserView($this,'validationInscription');
    $view->setArg('pseudo',$request->read('pseudo'));
    $view->render();
  }

  public function validationConnexion($request){
    $request->write('action','defaultAction');
    $this->defaultAction($request);
  }

  public function disconnectUser($request){
    $newRequest = Request::getCurrentRequest();
    $newRequest->write('action','disconnectUser');
    $newRequest->clearCookie('controller');
    $newRequest->clearCookie('game_id');
    $newRequest->clearCookie('pseudo');
    try {
      $controller = Dispatcher::dispatch($newRequest);
      $controller->execute();
    } catch (Exception $e) {
      echo 'Error : ' . $e->getMessage() . "\n";
    }
  }

  public function createGame($request){
    $view = new UserView($this,'createGame');
    $view->setArg('pseudo',$request->read('pseudo'));
    if (!is_null($request->read('message'))) {
      $view->setArg('message',$request->read('message'));
    }
    $view->render();
  }

  public function deleteGameSubscription($request){
    $playerId = Player::getPlayerId($request->read('game_id'),$request->read('pseudo'));
    $playerIsHost = Player::isPlayerAnHost($request->read('pseudo'),$request->read('game_id'));
    $playersOnGame = Player::getPlayersOnAGame($request->read('game_id'));
    if (count($playersOnGame)==1) {
      Player::deleteASinglePlayerFromAGame($playerId,$request->read('game_id'));
      Game::deleteGame($request->read('game_id'));
    } else if ($playerIsHost){
      $player=$playersOnGame[0];
      $counter=0;
      $pseudoOfPlayer = $player['login'];
      $substituteId = $playerId;
      while ($playerId==$substituteId) {
        $counter++;
        $player=$playersOnGame[$counter];
        $pseudoOfPlayer = $player['login'];
        $substituteId = Player::getPlayerId($request->read('game_id'),$pseudoOfPlayer);
      }
      Player::switchHost($request->read('game_id'),$playerId,$substituteId);
      Player::deleteASinglePlayerFromAGame($playerId,$request->read('game_id'));
    } else{
      Player::deleteASinglePlayerFromAGame($playerId,$request->read('game_id'));
    }
    $request->write('action','defaultAction');
    $this->defaultAction($request);

  }

  public function launchGame($request){
    if (strlen($request->read('game_name'))==0) {
      $request->write('action','createGame');
      $request->write('message',$this->generateErrorMessages('fields_missing'));
      $this->createGame($request);
    } else {
      if(Game::enterGameInDatabase($request->read('game_name'),$request->read('gameSetting'))){
        $game_id=Game::getGameIdOfGameJustCreated();
        if (!is_null($game_id)){
          if (!is_null(Player::createPlayer($request->read('pseudo'),$game_id,1,0))) {
            if(!is_null(Player::changePlayerState($request->read('pseudo'),$game_id,1))){
              $request->write('action','waitingRoom');
              $request->write('game_id',$game_id);
              $request->write('game_name',$request->read('game_name'));
              if ($request->read('gameSetting')==0) {
                $request->write('message',$this->generateErrorMessages('just_created_private'));
              } else {
                $request->write('message',$this->generateErrorMessages('just_created_public'));
              }
              $this->waitingRoom($request);
            }
          }
        }
      }
    }
  }

  public function invitePlayers($request){
    $view= new UserView($this,'invitePlayers');
    $view->setArg('pseudo',$request->read('pseudo'));
    $view->setArg('game_id',$request->read('game_id'));
    $view->setArg('game_name',Game::getGameName($request->read('game_id')));
    $view->render();
  }

  public function inviteAnotherUser($request){
    //TODO : gérer le cas où le résultat de la recherche d'utilisateur ne renvoie rien.
    if (strlen($request->read('guestLogin'))==0) {
      $view=new UserView($this,'inviteAnotherUser');
      $view->setArg('game_id',$request->read('game_id'));
      $view->setArg('game_name',Game::getGameName($request->read('game_id')));
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->setArg('message',$this->generateErrorMessages('fields_missing'));
      $view->render();
    } elseif (strcmp($request->read('guestLogin'),$request->read('pseudo'))==0) {
      $view=new UserView($this,'inviteAnotherUser');
      $view->setArg('game_id',$request->read('game_id'));
      $view->setArg('game_name',Game::getGameName($request->read('game_id')));
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->setArg('message',$this->generateErrorMessages('you_are_not_a_guest'));
      $view->render();
    } else {
      $userWithThatNickname = User::isThereAnUserWithThatNickname($request->read('guestLogin'));
      if ($userWithThatNickname!=1) {
        $testName=$request->read('guestLogin');
        $letter=$testName[0];
        $tableau = User::gettingNickNamesThatBeginsWith($letter);
        $request->write('action','errorInInvitation');
        $view=new UserView($this,'errorInInvitation');
        $view->setArg('game_id',$request->read('game_id'));
        $view->setArg('table_of_pseudos',$tableau);
        $view->setArg('pseudo',$request->read('pseudo'));
        $view->setArg('error_in_pseudo',1);
        $view->render();
      } else{
        Player::createPlayer($request->read('guestLogin'),$request->read('game_id'),0,1);
        $request->write('game_name',Game::getGameName($request->read('game_id')));
        $request->write('action','waitingRoom');
        $this->waitingRoom($request);
      }
    }
  }

  public function buildingNewGame($request){
    if(Board::checkifGameLaunched($request->read('game_id')) && !Hand::areHandsEmpty($request->read('game_id'))){
      $request->write('action','buildingLaunchedGame');
      $this->buildingLaunchedGame($request);
    } else{
      //First of all, let's do the cleanup
      Stack::deleteWholeStack($request->read('game_id'));
      Hand::deleteWholeHand($request->read('game_id'));
      Board::deleteWholeBoard($request->read('game_id'));
      Game::changeGameState($request->read('game_id'),1);
      $listOfValues=$this->createArrayofValues();
      $cardsOnBoard = array_rand($listOfValues,4);
      for ($i=1; $i < 5; $i++) {
        Board::addCardToBoard($request->read('game_id'),$i,$cardsOnBoard[$i-1]+1);
        unset($listOfValues[$cardsOnBoard[$i-1]]);
      }
      $listOfPlayers = Player::getPlayersOnAGame($request->read('game_id'));
      for ($i=0; $i < count($listOfPlayers) ; $i++) {
        $playerArray=$listOfPlayers[$i];
        $playerId=Player::getPlayerId($request->read('game_id'),$playerArray['login']);
        $playerHand = array_rand($listOfValues,10);
        for ($j=0; $j <10 ; $j++) {
          Hand::insertCardInHand($request->read('game_id'),$playerHand[$j]+1,$playerId);
          unset($listOfValues[$playerHand[$j]]);
        }
      }
      $request->write('action','buildingLaunchedGame');
      $this->buildingLaunchedGame($request);
    }
  }

  public function playWithCard($request){
    //Attention : on ne peut pas supprimer la carte de la main du joueur sinon on ne sait plus qui l'a posée !
    if (!is_null(Board::addCardToBoard($request->read('game_id'),5,$request->read('card_id')))) {
      $request->write('action','checkingBoardContent');
      $this->checkingBoardContent($request);
    }
  }

  public function numberOfCardsOnRow($request){
    $row = $request->read('row_to_count');
    $counter = 0;
    for ($i=0; $i <count($row) ; $i++) {
      $card = $row[$i];
      if ($card['card']!=0) {
        $counter++;
      }
    }
    return $counter;
  }

  public function checkingBoardContent($request){
    $board = Board::getCardFromBoard($request->read('game_id'));
    $test = 0;
    if (isset($board[4])) {
      $request->write('row_to_count',$board[4]);
      $test = $this->numberOfCardsOnRow($request);
    } if($test<count(Player::getPlayersOnAGame($request->read('game_id')))){
      $request->write('action','buildingLaunchedGame');
      $playerId=Player::getPlayerId($request->read('game_id'),$request->read('pseudo'));
      $playerAlreadyPlayed = 0;
      if (isset($board[4])) {
        $row = $board[4];
        $counter=0;
        while ($playerAlreadyPlayed==0 && $counter<count($row)) {
          $card=$row[$counter];
          if ($card['card']!=0) {
            $ownerId=Hand::whoOwnsTheCard($request->read('game_id'),$card['card']);
            if (!is_null($ownerId)) {
              if($ownerId==$playerId){
                $playerAlreadyPlayed=1;
              }
            }
          }
          $counter++;
        }
      }
      if ($playerAlreadyPlayed==1) {
        $request->write('waiting_for_others',1);
      } else{
        $request->write('waiting_for_others',0);
      }
      $this->buildingLaunchedGame($request);
    } else{
      $request->write('action','launchTheCalculus');
      $request->write('waiting_for_others',0);
      $request->write('board',$board);
      $this->launchTheCalculus($request);
    }
  }

  public function launchTheCalculus($request){
    $board = $request->read('board');
    // Récupérer les cartes sur le rang 5.
    $powerOfRows = Board::getScoreOfRow($request->read('game_id'));
    $rowFive = $board[count($board)-1];
    // Trier ces cartes avec le minimum en premier.
    // Récupérer les valeurs maximales sur les 4 premières rangées.
    $maximumValuesOnRow = Board::getMaximalValueOfRows($request->read('game_id'));
    for ($i=0; $i < count(Player::getPlayersOnAGame($request->read('game_id'))); $i++) {
      //On va récupérer l'information : à qui appartient la carte ?
      $currentCard = $rowFive[$i];
      $owner = Hand::whoOwnsTheCard($request->read('game_id'),$currentCard['card']);
      // Puis boucle : la carte est-elle inférieure à l'ensemble des maximas ?
      if ($this->smallerThanAllMaximas($currentCard['card'],$maximumValuesOnRow)) {
        // Si oui, on pose sur la rangée qui vaut le moins de points (ou le premier résultat si égalité)
        $index = $this->getLeastPowerfulRow($powerOfRows);
        // Puis appel à la fonction emptyingaRow
        $request->write('card_owner',$owner);
        $request->write('row_to_update',$index);
        $request->write('card_to_insert',$currentCard['card']);
        $this->emptyingARow($request);
      } else{
        // Si non, calcul des écarts entre la carte et les maximas, on prend l'écart le plus petit.
        $indexWithMinimalGap = $this->getSmallestGapBetweenCards($currentCard['card'],$maximumValuesOnRow);
        $request->write('row_to_count',$board[$indexWithMinimalGap-1]);
        $rowLength=$this->numberOfCardsOnRow($request);
        // On récupère la longueur de la rangée en question.
        if ($rowLength==5) {
          // Si la longueur vaut 5, on appelle emptyingaRow.
          $request->write('card_owner',$owner);
          $request->write('row_to_update',$indexWithMinimalGap);
          $request->write('card_to_insert',$currentCard['card']);
          $this->emptyingARow($request);
        } else{
          // Sinon, on pose la carte au sein de ce rang.
          Board::updateCardOnBoard($request->read('game_id'),$indexWithMinimalGap,$currentCard['card']);
        }
      }
      Hand::deleteCardFromHand($request->read('game_id'),$currentCard['card']);
      $board = Board::getCardFromBoard($request->read('game_id'));
      $powerOfRows = Board::getScoreOfRow($request->read('game_id'));
      $maximumValuesOnRow = Board::getMaximalValueOfRows($request->read('game_id'));
    }
    $request->write('action','buildingLaunchedGame');
    $this->buildingLaunchedGame($request);
  }

  public function emptyingARow($request){
    //vide le contenu d'une rangée, met à jour le score et insère la première carte.
    $powerOfRows = Board::getScoreOfRow($request->read('game_id'));
    $pointsToAdd = $powerOfRows[$request->read('row_to_update')-1];
    Player::updatePlayerScore($request->read('game_id'),$request->read('card_owner'),$pointsToAdd);
    $arrayOfCardsToMove = Board::getCardsFromARow($request->read('game_id'),$request->read('row_to_update'));
    for ($i=0; $i < count($arrayOfCardsToMove); $i++) {
      Stack::insertCardInStack($request->read('game_id'),$request->read('card_owner'),$arrayOfCardsToMove[$i]);
    }
    Board::deleteRowFromBoard($request->read('game_id'),$request->read('row_to_update'));
    Board::updateCardOnBoard($request->read('game_id'),$request->read('row_to_update'),$request->read('card_to_insert'));
    return 0;
  }

  public function getSmallestGapBetweenCards($currentCard,$array_of_maximas){
    $index = 0;
    $gap=104;
    for ($i=0; $i < count($array_of_maximas); $i++) {
      if ($currentCard-$array_of_maximas[$i]<$gap && $currentCard-$array_of_maximas[$i]>0) {
        $gap = $currentCard-$array_of_maximas[$i];
        $index= $i;
      }
    }
    return $index+1;
  }

  public function getLeastPowerfulRow($array_of_scores){
    $index=0;
    $score=666;
    for ($i=0; $i < count($array_of_scores); $i++) {
      if ($array_of_scores[$i]<$score) {
        $index = $i;
        $score = $array_of_scores[$i];
      }
    }
    return $index+1;
  }

  public function smallerThanAllMaximas($value,$array_of_maximas){
    $counter=0;
    for ($i=0; $i < count($array_of_maximas); $i++) {
      if ($value<$array_of_maximas[$i]) {
        $counter++;
      }
    }
    if ($counter == count($array_of_maximas)) {
      return true;
    }
    return false;
  }

  public function buildingLaunchedGame($request){
    $request->writeCookie('game_id',$request->read('game_id'));
    $playerId = Player::getPlayerId($request->read('game_id'),$request->read('pseudo'));
    $arrayOfScores = Player::getPlayersScore($request->read('game_id'));
    if ($this->scoreGreaterThanSixtySix($arrayOfScores)) {
      $request->write('action','gameOver');
      $request->write('player_id',$playerId);
      $this->gameOver($request);
    } else{
      $view= new UserView($this,'buildingLaunchedGame');
      $message = '';
      if (Hand::areHandsEmpty($request->read('game_id'))) {
        $view->setArg('new_round_required',1);
        if (Player::isPlayerAnHost($request->read('pseudo'),$request->read('game_id'))) {
          $message = $message.$this->generateErrorMessages('new_round_required_host');
          $view->setArg('player_is_the_host',1);
        } else{
          $message = $message.$this->generateErrorMessages('new_round_required_guest');
        }
      }
      if ($request->read('waiting_for_others')==1) {
        $view->setArg('waiting_for_others',$request->read('waiting_for_others'));
        $message = $message.$this->generateErrorMessages('waiting_for_others');
      }
      if (User::getNumberOfGamesPlayed($request->read('pseudo'))==0){
        $message = $message.$this->generateErrorMessages('first_time');
      }
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->setArg('board',Board::getCardFromBoard($request->read('game_id')));
      $view->setArg('hand',Hand::getCardFromHand($request->read('game_id'),$playerId));
      $view->setArg('score',Player::getPlayersScore($request->read('game_id')));
      $view->setArg('scores_in_html',Player::getAllPlayersScoreInHTML(Player::getPlayersScore($request->read('game_id'))));
      $view->setArg('row_power',Board::getScoreOfRow($request->read('game_id')));
      $view->setArg('message',$message);
      $view->setArg('game_id',$request->read('game_id'));
      $view->render();
    }
  }

  private function scoreGreaterThanSixtySix($arrayOfScores){
    for ($i=0; $i < count($arrayOfScores); $i++) {
      $playerScore=$arrayOfScores[$i];
      if($playerScore['score']>15){
        return true;
      }
    }
    return false;
  }

  private function createArrayofValues(){
    $res = array();
    for ($i=1; $i <105 ; $i++) {
      array_push($res,$i);
    }
    return $res;
  }

  public function gameOver($request){
    Game::changeGameState($request->read('game_id'),0);
    $view = new UserView($this,'gameOver');
    $view->setArg('player_id',$request->read('player_id'));
    $view->setArg('game_name',Game::getGameName($request->read('game_id')));
    $view->setArg('game_id',$request->read('game_id'));
    $view->setArg('pseudo',$request->read('pseudo'));
    $view->setArg('list_scores',Player::getPlayersScore($request->read('game_id')));
    $view->render();
  }

  public function updatingGameStatus($request){
    Player::changePlayerState($request->read('pseudo'),$request->read('game_id'),0);
    if (Player::getNumberOfPlayersReady($request->read('game_id'))==0) {
      $array_of_scores = Player::getPlayersScore($request->read('game_id'));
	  //MOuahaha vous avez fait F5... apprenez à utiliser les boutons bordel !!! ; )))))))))))))))
      $winner = $array_of_scores[0];
      User::updateGamesWon($winner['pseudo']);
      for ($i=0; $i < count($array_of_scores); $i++) {
        $player = $array_of_scores[$i];
        User::updateGamesPlayed($player['pseudo']);
        User::updateAverageScore($player['pseudo'],$player['score']);
      }
      Stack::deleteWholeStack($request->read('game_id'));
      Hand::deleteWholeHand($request->read('game_id'));
      Board::deleteWholeBoard($request->read('game_id'));
      Player::deleteAllPlayersFromAGame($request->read('game_id'));
      Game::deleteGame($request->read('game_id'));
    }
    $request->clearCookie('game_id',$request->read('game_id'));
    $request->write('action','defaultAction');
    $this->defaultAction($request);
  }

  public function passwordChangeDone($request){
    if (strlen($request->read('newPassword'))==0 || strlen($request->read('oldPassword'))==0 || strlen($request->read('confirmNewPassword'))==0 ) {
      $request->write('action','changePassword');
      $view = new UserView($this,'changePassword');
      $view->setArg('message',$this->generateErrorMessages('fields_missing'));
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->render();
    } elseif (strcmp($request->read('newPassword'),$request->read('confirmNewPassword'))!=0) {
      $request->write('action','changePassword');
      $view = new UserView($this,'changePassword');
      if (strlen($request->read('newPassword'))>15) {
        $view->setArg('message',$this->generateErrorMessages('passwords_not_equal_failed'));
      } else {
        $view->setArg('message',$this->generateErrorMessages('passwords_not_equal'));
      }
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->render();
    } else{
      $changePassword = User::changePassword($request->read('pseudo'),$request->read('newPassword'),$request->read('oldPassword'));
      if ($changePassword) {
        $request->write('message',$this->generateErrorMessages('password_changed'));
        $request->write('action','defaultAction');
        $this->defaultAction($request);
      } else{
        $request->write('action','changePassword');
        $view = new UserView($this,'changePassword');
        $view->setArg('message',$this->generateErrorMessages('error_old_password'));
        $view->setArg('pseudo',$request->read('pseudo'));
        $view->render();
      }
    }
  }

  public function confirmDeletion($request){
    if (Player::deleteThePlayerFromAllGamesHeIsInvolved($request->read('pseudo')) && User::delete($request->read('pseudo'))) {
      $request = Request::getCurrentRequest();
      $request->write('message',$this->generateErrorMessages('good_bye'));
      $request->clearCookie('controller');
      $request->clearCookie('pseudo');
      try{
        $controller = Dispatcher::dispatch($request);
        $controller->execute();
      }catch(Exception $e){
        echo 'Error : ' . $e->getMessage() . "\n";
      }
    } else {
      $request->write('action','deleteAccount');
      $view = new UserView($this,'deleteAccount');
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->setArg('message',$this->generateErrorMessages('error_on_deletion'));
      $view->render();
    }
  }

  public function changePassword($request){
    $request->writeCookie('pseudo',$_COOKIE['pseudo']);
    $view = new UserView($this,'changePassword');
    $view->setArg('pseudo',$request->read('pseudo'));
    $view->render();
  }

  public function rankings($request){
    $view = new UserView($this,'rankings');
    $view->setArg('pseudo',$request->read('pseudo'));
    $view->render();
  }

  public function deleteAccount($request){
    $view = new UserView($this,'deleteAccount');
    $view->setArg('pseudo',$request->read('pseudo'));
    $view->render();
  }

  public function playerIsReady($request){
    if (!is_null(Player::changePlayerState($request->read('pseudo'),$request->read('game_id'),1))) {
      $request->write('action','waitingRoom');
      //$request->write('player_ready'1);
      $this->waitingRoom($request);
    } else{
      $request->write('action','defaultAction');
      $view = new UserView($this,'defaultAction');
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->setArg('message',$this->generateErrorMessages('error_game_info'));
      $view->render();
    }
  }

  public function readRules($request){
    $view = new UserView ($this,'readRules');
    $view->setArg('button',$this->generateButtonReadRules($request->read('counter'),$request));
    $view->setArg('pseudo',$request->read('pseudo'));
    $view->setArg('counter',$request->read('counter'));
    $view->render();
  }

  public function waitingRoom($request){
    if (!is_null(Player::getPlayersOnAGame($request->read('game_id')))) {
      if (!Player::isPlayerAlreadyOnGame($request->read('pseudo'))) {
        Player::createPlayer($request->read('pseudo'),$request->read('game_id'),0,0);
      }
      $result_get_players = Player::getPlayersOnAGame($request->read('game_id'));
      $counter = 0;
      for ($i=0; $i < count($result_get_players); $i++) {
        $player = $result_get_players[$i];
        if ($player['ready']==1) {
          $counter++;
        }
      }
      $view = new UserView($this,'waitingRoom');
      $view->setArg('nb_ready',$counter);
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->setArg('game_players',$result_get_players);
      $view->setArg('game_name',$request->read('game_name'));
      $view->setArg('game_id',$request->read('game_id'));
      if (strlen($request->read('message'))>0) {
        $view->setArg('message',$request->read('message'));
      }
      if (count($result_get_players)==10) {
        $view->setArg('hide_invitation',1);
      }
      if (count($result_get_players)==0) {
        $view->setArg('message',$this->generateErrorMessages('error_game_info'));
      }
      if(Board::checkifGameLaunched($request->read('game_id'))){
        $view->setArg('game_launched',1);
      }
      if (!is_null(Player::isPlayerReady($request->read('pseudo'),$request->read('game_id')))) {
        $view->setArg('player_ready',Player::isPlayerReady($request->read('pseudo'),$request->read('game_id')));
      }
      $view->render();
    } else{
      $request->write('action','defaultAction');
      $view = new UserView($this,'defaultAction');
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->render();
    }
  }

  public function classement($req){
    $view = new UserView($this,'classement');
    $view->setArg('pseudo',$req->read('pseudo'));
    $login = $req->read('Login');
    if($login != ''){
      $data = Classement::recherche_pseudo($login);
      $view->setArg('recherche_pseudo',$data);
    }
    else{
      $page = $req->read('Page');
      if($page==''){$page=1;}//Si pas de page spécifique, page 1.
      $order = $req->read('Order');

      $champ = 0;
      switch($order){
        case 'Toutes les statistiques' : $order='';$champ=1;break;
        case 'Parties jouees' : $order='partie_jouee';$champ=2;break;
        case 'Parties gagnees' : $order='partie_gagnee';$champ=3;break;
        case 'Scores' : $order='score_moyen';$champ=4;break;
      }
      $view->setArg('selection',$champ);
      $data = Classement::recherche($order, ($page-1)*10, $page*10-1);
      $view->setArg('recherche',$data);
    }
    $view->render();
  }

  public function generateErrorMessages($keyString){
    $arrayOfMessages = array( 'error_game_info' => '<div class="alert alert-warning" role="alert">Erreur à la récupération des informations liées à la partie. Celle-ci n\'existe probablement pas.</div>'
    , 'new_round_required_host' => '<div class="alert alert-info" role="alert">Nous sommes arrivés au terme de ce round. Veuillez cliquer sur le bouton Tour suivant pour continuer à jouer.</div>'
    , 'new_round_required_guest' => '<div class="alert alert-info" role="alert">Nous sommes arrivés au terme de ce round. Veuillez cliquer sur le bouton Actualiser pour rafraîchir le jeu.</div>'
    , 'waiting_for_others' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Votre choix a été pris en compte. Nous attendons les autres joueurs. Cliquez régulièrement sur actualiser.</div>'
    , 'error_old_password' => '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Votre ancien mot de passe n\'est pas correct.</div>'
    , 'fields_missing' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Complétez tous les champs.</div>'
    , 'password_too_long' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Merci de saisir un mot de passe entre 1 et 15 caractères.</div>'
    , 'password_changed' => '<div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Le mot de passe a été changé avec succès !</div>'
    , 'error_on_deletion' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Nous n\'avons pas pu supprimer votre compte. Merci de réessayer.</div>'
    , 'no_games_available' => '<div class="alert alert-info" role="alert">Aucune partie disponible. <strong><a href="index.php?action=createGame" classe="alert-link">Créez votre partie !</a></strong></div>'
    , 'error_in_stats' => '<div class="alert alert-warning" role="alert">Impossible de récupérer vos statistiques.</div>'
    , 'passwords_not_equal' => '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Votre mot de passe et sa confirmation ne correspondent pas.</div>'
    , 'passwords_not_equal_failed' => '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Votre mot de passe et sa confirmation ne correspondent pas. <br> Merci de saisir un mot de passe entre 1 et 15 caractères.</div>'
    , 'first_time' => '<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> C\'est votre premier jeu ! <a href="index.php?action=readRules&counter=1" class="alert-link">Vous trouverez les règles en cliquant ici !</a>.</div>'
    , 'just_created_public' => '<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Votre partie <strong>publique</strong> a bien été créée. Le jeu pourra être lancé s\'il contient 2 à 10 joueurs et que tout le monde est prêt. <strong>Actualisez</strong> la page pour voir vos concurrents arriver ou <strong>invitez-les</strong> !</div>'
    , 'just_created_private' => '<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Votre partie <strong>privée</strong> a bien été créée. Le jeu pourra être lancé s\'il contient 2 à 10 joueurs et que tout le monde est prêt. <strong>Invitez</strong> des concurrents et <strong> actualisez</strong> régulièrement la page !</div>'
    , 'good_bye' => '<div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Votre compte a bien été supprimé.</div>'
    , 'you_are_not_a_guest' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Vous ne pouvez pas vous inviter à jouer !</div>'
    , 'play_to_show_stats' => '<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Jouez pour pouvoir voir vos statistiques.</div>'
  );
  return $arrayOfMessages[$keyString];
}

public function generateButton($beginWithGameAtRow,$keyString){
  switch ($keyString) {
    case 'previous':
    return '<a class="btn btn-default" href="index.php?action=defaultAction&beginWith='.($beginWithGameAtRow-5).'" role="button"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span></a>';
    break;
    default:
    return '<a class="btn btn-default" href="index.php?action=defaultAction&beginWith='.($beginWithGameAtRow+5).'" role="button"><span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>';
    break;
  }
}

public function generateButtonReadRules($indexCalled,$request){
  if ($indexCalled==1) {
    $finalString ='';
    if (strlen($request->read('game_id'))>0) {
      $finalString = '<li class="previous"><a href="index.php?action=buildingLaunchedGame"><strong><span aria-hidden="true">&larr;</span> Retour au jeu</strong></a></li>';
    }
    $finalString = $finalString. '<li class="next"><a href="index.php?action=readRules&counter=2">Suivant <span aria-hidden="true">&rarr;</span></a></li>';
    return $finalString;
  } elseif ($indexCalled==4) {
  $finalString = '<li class="previous"><a href="index.php?action=readRules&counter=3"><span aria-hidden="true">&larr;</span> Précédent</a></li>';
  if (strlen($request->read('game_id'))>0) {
    $finalString = $finalString. '<li class="next"><a href="index.php?action=buildingLaunchedGame"><strong>Retour au jeu <span aria-hidden="true">&rarr;</span></strong></a></li>';
  }
  return $finalString;
} else{
  return '<li class="previous"><a href="index.php?action=readRules&counter='.($indexCalled-1).'"><span aria-hidden="true">&larr;</span> Précédent</a></li><li class="next"><a href="index.php?action=readRules&counter='.($indexCalled+1).'">Suivant <span aria-hidden="true">&rarr;</span></a></li>';
}
}

}

?>
