<?php
/**
 *
 */
abstract class Controller extends MyObject{
    protected $request;

    public function __construct($request_b){
        $this->request = $request_b;
    }

    public abstract function defaultAction($request);

    public function execute(){
	    $req = $this->request;
		$nameOfAction = $this->request->getActionName();
		if(method_exists($this, $nameOfAction)){
			//Si la fonction demandée existe, on y va. Sinon, defaultAction.
			$this->$nameOfAction($req);
		}
		else{
			$this->defaultAction($req);
		}
		//$this->$nameOfAction($req);
    }

}
 ?>
