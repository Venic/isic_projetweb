<?php
/**
* List  of all functions used in this Controller
* Name of the function          Type of element returned        Purpose of the function
* defaultAction                 void                            Prepares the defaultView un Anonymous mode : authentication panel.
* inscription                   void                            Prepares the subscription template.
* validationInscription         void                            Checks if all the parameters requested for the user's susbscription are satisfied.
* checkLengthOfInput            int                             Checks if the chain entered as an input matched the length of the type of information it represents.
* validateInscription           void                            Does the necessary in order to create the account. Leads to a switch to a UserController.
* validationConnexion           void                            Checks if all the parameters requested for user's authentication are satisfied.
* authorizeConnection           void                            Does the necessary in order to authenticate the user. Leads to a switch to a UserController.
* disconnectUser                void                            Termination of actions that started in a UserController. Disconnects the user.
* classement                    void                            Does the process in order to display the rankings.
* generateErrorMessages         string                          Returns the correct alert message requested by the key string given in parameter.
*/
class AnonymousController extends Controller{

  function __construct($request){
    parent::__construct($request);
	//On triche un peu en écrivant le controller
  }

  public function defaultAction($request){
	//Action pas défaut
    $view = new AnonymousView($this,'defaultAction');
    if (strlen($request->read('message'))>0) {
      $view->setArg('message',$request->read('message'));
    }
    $view->render();
  }

  public function inscription($request){
    $view = new AnonymousView($this,'inscription');
    $view->render();
  }

  public function validationInscription($request){
    if (strcmp($request->read('inscPassword'),$request->read('confirmPassword'))!=0){
      $view = new AnonymousView($this,'inscription');
      if ($this->checkLengthOfInput($request->read('inscPassword'),'password')==0) {
        $view->setArg('message',$this->generateErrorMessages('passwords_not_equal_failed'));
      } else{
        $view->setArg('message',$this->generateErrorMessages('passwords_not_equal'));
      }
      $view->render();
    } else{
      $check = $this->checkLengthOfInput($request->read('inscPassword'),'password') + 2*$this->checkLengthOfInput($request->read('inscLogin'),'pseudo');
      switch ($check) {
        case 3:
        $this->validateInscription($request);
          break;
        case 2:
        $view = new AnonymousView($this,'inscription');
        $view->setArg('message',$this->generateErrorMessages('password_failed'));
        $view->render();
          break;
          case 1:
          $view = new AnonymousView($this,'inscription');
          $view->setArg('message',$this->generateErrorMessages('pseudo_failed'));
          $view->render();
            break;
        default:
        $view = new AnonymousView($this,'inscription');
        $view->setArg('message',$this->generateErrorMessages('pseudo_password_failed'));
        $view->render();
          break;
      }
    }
  }

  public function checkLengthOfInput($input,$keyString){
    $arrayOfValues = array('pseudo' => 12, 'password' => 15 );
    if (strlen($input)==0 || strlen($input)>$arrayOfValues[$keyString]) {
      return 0;
    }
    return 1;
  }

  public function validateInscription($req){
    $login = $req->read('inscLogin');
	$password = $req->read('inscPassword');
    if(User::isLoginUsed($login)) {
		//On regarde si le pseudo est déjà utilisé.
      $view = new AnonymousView($this,'inscription');
      $view->setArg('message',$this->generateErrorMessages('login_used'));
      $view->render();
    } else {
      $user = User::create($login, $password);//On crée un compte.
      if(!isset($user)) {
		//Echec de la création.
        $view = new AnonymousView($this,'inscription');
        $view->setArg('message',$this->generateErrorMessages('inscription_failed'));
        $view->render();
      } else {
		//Succès de la création.
        $newRequest=Request::getCurrentRequest();
        $newRequest->write('action','defaultAction');
        $newRequest->writeCookie("controller",'User');
        $newRequest->write('message',$this->generateErrorMessages('welcome'));
        $newRequest->writeCookie("pseudo",$login);
        try {
          $controller = Dispatcher::dispatch($newRequest);
          $controller->execute();
        } catch (Exception $e) {
          echo 'Error : ' . $e->getMessage() . "\n";
        }
      }
    }

  }

  public function validationConnexion($request){
    if(!User::isLoginUsed($request->read('inscLogin'))) {
      $view = new AnonymousView($this,'inscription');
      $view->setArg('message',$this->generateErrorMessages('account_not_found'));
      $view->setArg('pseudo',$request->read('inscLogin'));
      $view->render();
    } elseif (!$request->read('inscLogin')==null && !$request->read('inscPassword')==null) {
		//On regarde si on nous a bien donné un pseudo et un mot de passe.
      $this->authorizeConnection($request);
    } else{
      $view = new AnonymousView($this,'defaultAction');
      $view->setArg('pseudo',$request->read('pseudo'));
      $view->setArg('message',$this->generateErrorMessages('fields_missing'));
      $view->render();
    }
  }

  public function authorizeConnection($request){
    $login = $request->read('inscLogin');
    $pwd = $request->read('inscPassword');
    $loginAndPasswordFound= User::checkIfLoginPresent($login,$pwd);//On regarde si l'identification est ok.
    if (isset($loginAndPasswordFound)) {
		//On connecte l'user.
      $newRequest=Request::getCurrentRequest();
      $newRequest->write('action','defaultAction');
      $newRequest->writeCookie("controller",'User');
      $newRequest->writeCookie("pseudo",$login);
      try {
        $controller = Dispatcher::dispatch($newRequest);
        $controller->execute();
      } catch (Exception $e) {
        echo 'Error : ' . $e->getMessage() . "\n";
      }
    } else{
      //echo "Failure in connection";
      $view = new AnonymousView($this,'defaultAction');
      $view->setArg('pseudo',$request->read('inscLogin'));
      $view->setArg('message',$this->generateErrorMessages('wrong_password'));
      $view->render();
    }
  }

  public function disconnectUser($request){
    $view = new AnonymousView($this,'defaultAction');
    $view->setArg('message',$this->generateErrorMessages('disconnect'));
    $view->render();
  }

  public function readRules($request){
    $view = new AnonymousView ($this,'readRules');
    $view->setArg('button',$this->generateButtonReadRules($request->read('counter'),$request));
    $view->setArg('counter',$request->read('counter'));
    $view->render();
  }


  public function classement($req){
    $view = new AnonymousView($this,'classement');

    $data1 = Classement::recherche('partie_jouee', 0, 15);
	$data2 = Classement::recherche('partie_gagnee', 0, 15);
	$data3 = Classement::recherche('score_moyen', 0, 15);
	$view->setArg('recherche_jouee',$data1);
	$view->setArg('recherche_gagnee',$data2);
    $view->setArg('recherche_score',$data3);

    $view->render();
  }


  public function generateErrorMessages($keyString){
  $arrayOfMessages = array('disconnect' =>'<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Vous êtes maintenant déconnecté</div>'
, 'wrong_password' => '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Vérifiez votre combinaison login/mot de passe.</div>'
, 'fields_missing'=> '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Complétez tous les champs.</div>'
, 'login_used' => '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Ce pseudo est déjà utilisé. Merci d\'en choisir un autre.</div>'
, 'inscription_failed' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>L\'inscription a échoué. Merci de retenter.</div>'
, 'pseudo_password_failed' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Merci de saisir un login entre 1 et 12 caractères.<br> Merci de saisir un mot de passe entre 1 et 15 caractères.</div>'
, 'pseudo_failed' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Merci de saisir un login entre 1 et 12 caractères.</div>'
, 'password_failed' => '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Merci de saisir un mot de passe entre 1 et 15 caractères.</div>'
, 'passwords_not_equal' => '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Votre mot de passe et sa confirmation ne correspondent pas.</div>'
, 'passwords_not_equal_failed' => '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Votre mot de passe et sa confirmation ne correspondent pas. <br> Merci de saisir un mot de passe entre 1 et 15 caractères.</div>'
, 'account_not_found' => '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Compte inexistant. Merci de créer votre compte.</div>'
, 'welcome' => '<div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <strong>Bienvenue !</strong> Voici votre page d\'accueil. Vous trouverez <strong>ci-dessous le tableau des parties publiques disponibles</strong>. Lorsque vous commencerez
à jouer, ce tableau affichera vos parties en cours et les parties auxquelles vous avez été invité. Vous pouvez à l\'aide du menu en haut <a href="index.php?action=createGame" class="alert-link">créer une partie</a>, <a href="index.php?action=readRules&counter=1" class="alert-link">lire les règles</a>, consulter les classements &#8230; Bon jeu !</div>'
);
    return $arrayOfMessages[$keyString];
  }
  public function generateButtonReadRules($indexCalled,$request){
    if ($indexCalled==1) {
      return '<li class="next"><a href="index.php?action=readRules&counter=2">Suivant <span aria-hidden="true">&rarr;</span></a></li>';
    } elseif ($indexCalled==4) {
    return '<li class="previous"><a href="index.php?action=readRules&counter=3"><span aria-hidden="true">&larr;</span> Précédent</a></li>';
  } else{
    return '<li class="previous"><a href="index.php?action=readRules&counter='.($indexCalled-1).'"><span aria-hidden="true">&larr;</span> Précédent</a></li><li class="next"><a href="index.php?action=readRules&counter='.($indexCalled+1).'">Suivant <span aria-hidden="true">&rarr;</span></a></li>';
  }
  }

}
?>
