#README, LISEZ-MOI#

This file aims at giving you some technical explanations regarding the 6 nit! project realised by Romain Gendreau and Clément Venini at IMT Lille Douai, France.
_Ce fichier Lisez-moi a pour objectif de fournir des explications techniques à propos du projet 6 qui prend! réalisé par Romain Gendreau et Clément Venini alors à l’IMT Lille Douai, France._

### What is this repository for? ###

This repo enables you to run an online version of the 6 nimmt! game, which is in its initial format a board game, playable between 2 and 4 players. This project is not using a PHP framework as it was a requirement from the teachers.
_Ce dépôt vous permet de faire fonctionner une version en ligne du jeu 6 qui prend!, qui est initialement un jeu de plateau de 2 à 4 joueurs. Ce projet n’utilise pas de framework PHP comme il s’agissait d’une exigence des professeurs.


### How do I get set up? ###

You just need to make sure that the whole bundle of folders and files are in the public_html folder of your filesystem.
Your database must be operating with MySQL. The SQL file that allows you to create the database’s tables and the file that gives the correct values to the cards can be found in the sql folder. They are named respectively Creating database.sql and Test.sql.
Minor changes would have to be done on location of assets. Do remove http://localhost for correct operation.
_Vous devez vous assurer que l’ensemble des dossiers et fichiers sont dans le dossier public_html de votre arborescence.
Votre base de données doit fonctionner sous MySQL. Le fichier SQL vous permettant de créer la base de données et le fichier permettant d’assigner les valeurs aux cartes sont le dossier sql. Ils sont nommés Creating database.sql et Test.sql.
Des changements mineurs doivent être faits sur l’emplacement des assets. Retirez http://localhost dans les templates pour un fonctionnement correct._