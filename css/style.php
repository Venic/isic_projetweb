<?php
header("Content-type: text/css; charset: UTF-8");
date_default_timezone_set("Europe/Paris");
$primaryColor = 0;
$headlineColor = "#FFFFFF";
$textColor = "#636C7A";

$today=getdate();
if ($today['hours']>=21 || $today['hours']<5 ) {
  $primaryColor="#162265";
  $textColor = "#FFFFFF";
} else if ($today['hours']>=5 && $today['hours']<9) {
  $primaryColor="#E9A900";
} else if ($today['hours']>=9 && $today['hours']<14) {
  $headlineColor ="#000000";
  $primaryColor="#EAD3A2";
} else if ($today['hours']>=14 && $today['hours']<18) {
  $primaryColor="#00C1EA";
} else if ($today['hours']>=18 && $today['hours']<21) {
  $primaryColor="#207B8F";
  $textColor = "#FFFFFF";
  $headlineColor ="#FFFFFF";
}
?>

@import url("fonts.css");

.panel-title{
  color:#636C7A !important;
}

nav{
  font-family: 'metropolissemi_bold';
  /*  max-width: 97%;*/
}

.logo-6{
  text-align: center;
}

.logo-6 img{
  display: block;
  max-height: 12vh;
  margin-left: auto;
  margin-right: auto;
}

.game-rules img{
  display: block;
  max-height: 25vh;
  margin-left: auto;
  margin-right: auto;
}

p,li,.well-lg,h1,h2,h3,h4,h5,h6{
  animation-name: slidingIn !important;
  animation-duration:0.4s !important;
  animation-timing-function: ease-in-out !important;
}

.menu,.footer2{
  animation-duration:0s !important;

}

h1,h2,h3,h4,h5,h6{
  color:<?php echo $headlineColor; ?> !important;
  font-family: 'metropolismedium' !important;
}

@keyframes slidingIn{
  0% {opacity:0;margin-left:10vh;}
  25% {opacity:0.25;margin-left;7.5vh;}
  50% {opacity:0.5;margin-left:5vh;}
  75% {opacity:0.75;margin-left:2.5vh;}
  100% {opacity:1;margin-left:0vh;}
}

#disconnect{
  text-align: right;
}

header{
  margin-top: 10px;
  background-color: <?php  echo $primaryColor;?>;
  <!-- background-color: #8bc34a; -->
  line-height: 48px;
}

.navbar-brand img{
  height: 24px !important;
}

.row{
  padding-left: 10px;
  padding-right: 10px;
}

html{
  background-color: <?php  echo $primaryColor;?>;
}

body{
  font-family: 'metropolisregular' !important;
}
section{
  padding-top:2%;
  background: <?php  echo $primaryColor;?>;
  background-size: cover;
  /*min-height: 90vh;*/
}

footer{
  position: fixed;
  color:black !important;
  background-color: #F8F8F8;
  height: 5vh;
  line-height: 5vh;
  bottom:0px;
  left:0px;
  right:0px;
  margin-bottom:0px;
  text-align: center;
  box-shadow : 10px 10px 5px grey;
}

footer:hover{
  animation-name: footerShrink !important;
  animation-duration:3s !important;
}


footer p{
  text-align: center !important;
  line-height: 2vh;
}

@keyframes footerShrink{
  0% {height:5vh;}
  25% {height:13vh;}
  75% {height:13vh;}
  100% {height:5vh;}
}

.central_layer h1{
  color:black !important;
}

.central_layer{
  background-color: #F8F8F8;
  border-radius: 5px;
  max-width: 90vw;
  transform: translate(-50%,-50%);
  padding-left: 5vw;
  padding-right: 5vw;
  padding-top: 3vh;
  padding-bottom: 3vh;
  box-shadow: 2px 2px 5px #111111;
  position: absolute;
}

.box-homepage-user{
  position: relative;
  transform: translate(0%,0%);
}

.headline{
  position: relative;
  padding-left: 10vw;
  padding-right: 10vw;
}

.authenticate{
  top: 50%;
  left : 50%;
}

.central_layer .btn{
  min-width: 100%;
  margin-top: 5px;
  margin-bottom: 5px;
}

.menu-button{
  vertical-align: middle;
  display: table-cell;
}

.card{
  font-family: 'metropolisblack';
  display: inline-block;
  height: 8vh;
  width: 10vw;
  margin: 5px;
  background-color: white;
  box-shadow: 2px 2px 5px #111111;
  text-align: center;
  line-height: 8vh;
  font-size: 2.5em;
}

.card-0-pt{
  opacity: 0.4;
  background-color: black;
}


.card-1-pt{
  color: #7b1fa2;
  background-color:#F8F8F8;
}

.card-2-pt{
  background-color:#2196f3;
  color:#ffc107;
  text-shadow: 1px 1px 1px #000000;
}

.card-3-pt{
  background-color:#ffa000;
  color:white;
}

.card-5-pt{
  background-color:#ff5252;
  color:white;
}

.card-7-pt{
  background: black;
  /*background-color:#7b1fa2;*/
  color:#ffc107;
  text-shadow: 1px 1px 10px #ff5252;

}

.points-row{
  color: <?php  echo $textColor;?> !important;
  display: inline-block;
  width: 10vw;
  font-family: 'metropolisbold';
  font-size: 1.5em;
  margin: 5px;
}

.player-name-board{
  color: <?php  echo $textColor;?> !important;
  font-family: 'metropolislight';
  font-size: 0.8em;
}

.player-score-board{
  color: <?php  echo $textColor;?> !important;
  font-family: 'metropolismedium';
  font-size: 1.6em;
}

p,li{
  text-align: justify !important;
}

.game-rules-text{
  color: <?php  echo $textColor;?> !important;
}

.rules-well{
  text-align: center;
}

@media screen and (min-width: 768px) {
  #well-rules {
    min-height: 18vh !important;
  }

}
