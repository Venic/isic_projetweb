<?php

/**
* List of all functions used in this View subclass
* Name of the function          Type of element returned        Purpose of the function
* setArg                        void                            Gives an extra argument to the view that may be used in the files called by the functions.
* render                        void                            includes the head and footer for every single webpage + calls the appropriate rendering function
* defaultActionRender           void                            Displays the authentication panel.
* gameRulesRender               void                            Displays the game rules.
* inscriptionRender             void                            Displays the subscription template.
* classementRender              void                            Displays the rankiings.
*/
class AnonymousView extends View{

  function __construct($controller,$action){
    parent::__construct($controller,$action);
  }

	public function setArg($name,$arguments){
		$this->args[$name]= $arguments;
	}

  public function render(){
	$nameOfRenderDeux = $this->action.'Render';
    include __ROOT_DIR.'/templates/headTemplate.php';
    $this->$nameOfRenderDeux($this->args);
    include __ROOT_DIR.'/templates/footerTemplate.php';
  }

  public function defaultActionRender($args){
	  include __ROOT_DIR.'/templates/userNotConnected.php';
  }

  public function inscriptionRender($args){
      include __ROOT_DIR.'/templates/inscriptionTemplate.php';
  }

   public function classementRender($args){
	 include __ROOT_DIR.'/templates/headerNavTemplate.php';
    include __ROOT_DIR.'/templates/anonymousClassement.php';
  }

  public function readRulesRender($args){
    include __ROOT_DIR.'/templates/headerNavTemplate.php';
    include __ROOT_DIR.'/templates/howTo'.$args['counter'].'.php';
  }
}
  ?>
