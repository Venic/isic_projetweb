<?php

/**
* List of all functions used in this View subclass
* Name of the function          Type of element returned        Purpose of the function
* setArg                        void                            Gives an extra argument to the view that may be used in the files called by the functions.
* render                        void                            includes the head and footer for every single webpage + calls the appropriate rendering function
* errorInInvitationRender       void                            Displays the form in order to invite another player
* gameOverRender                void                            displays the final scores in a game.
* buildingLaunchedGameRender    void                            Displays the whole equipement needed during a game.
* waitingRoomRender             void                            Displays the waiting room, required before having the access to a game granted.
* defaultActionRender           void                            Defines the templates required in order to display the user's homepage.
* deleteAccountRender           void                            Displays the form in order to delete the account.
* invitePlayersRender           void                            Displays the form that allow a player to invite some other ones.
* inviteAnotherUserRender       void                            Same as the one above.
* changePasswordRender          void                            Displays the form in order the change the password.
* createGameRender              void                            Displays the form in order to create a new game.
* launchGameRender              void                            Displays the waiting room just after the player has decided to create a new game.
* classementRender              void                            Displays the rankings.
*/
class UserView extends View
{

  function __construct($controller,$action){
    parent::__construct($controller,$action);
  }

  public function setArg($name,$arguments){
    $this->args[$name]=$arguments;
  }

  public function render(){
    $request = (new Request)->getCurrentRequest();
    $nameOfRenderDeux = $request->getActionName().'Render';
    include __ROOT_DIR.'/templates/headTemplate.php';
    include __ROOT_DIR.'/templates/headerNavTemplateUser.php';
    $this->$nameOfRenderDeux($this->args);
    include __ROOT_DIR.'/templates/footerTemplate.php';
  }

  public function errorInInvitationRender($args){
    include __ROOT_DIR.'/templates/errorInInvitation.php';
  }

  public function gameOverRender($args){
    include __ROOT_DIR.'/templates/endOfGame.php';
  }

  public function buildingLaunchedGameRender($args){
    include __ROOT_DIR.'/templates/tableOfScores.php';
    include __ROOT_DIR.'/templates/partOfBoard.php';
    include __ROOT_DIR.'/templates/partOfBoardwithWell.php';
  }

  public function waitingRoomRender($args){
    include __ROOT_DIR.'/templates/waitingRoom.php';
  }

  public function defaultActionRender($args){
    include __ROOT_DIR.'/templates/availableGames.php';
    include __ROOT_DIR.'/templates/userHomepage.php';
  }

  public function deleteAccountRender($args){
    include __ROOT_DIR.'/templates/deleteAccount.php';
  }

  public function invitePlayersRender($args){
    include __ROOT_DIR.'/templates/invitationTemplate.php';
  }

  public function inviteAnotherUserRender($args){
    $this->invitePlayersRender($args);
  }

  public function changePasswordRender($args){
    include __ROOT_DIR.'/templates/changePassword.php';
  }

  public function validationInscriptionRender($args){
    $this->defaultActionRender($args);
  }

  public function createGameRender($args){
    include __ROOT_DIR.'/templates/createGame.php';
  }

public function launchGameRender($args){
  include __ROOT_DIR.'/templates/waitingRoom.php';
}

  public function classementRender($args){
    include __ROOT_DIR.'/templates/userClassement.php';
  }

public function readRulesRender($args){
  include __ROOT_DIR.'/templates/howTo'.$args['counter'].'.php';
}

}
?>
