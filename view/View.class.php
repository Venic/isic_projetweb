<?php

/**
 *
 */
abstract class View extends MyObject{
  protected $controller;
  protected $action;
  protected $args;

  function __construct($controller,$action){
    $this->controller=$controller;
    $this->action=$action;
  }

  abstract public function setArg($name,$arguments);
  
  abstract public function render();

}
 ?>
