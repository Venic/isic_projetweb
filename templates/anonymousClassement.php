<section>
  <div class="container">
  <h1>Classement</h1>
  </div>
<div class="container">
<?php
	$data1 = $this->args['recherche_jouee'];
	$data2 = $this->args['recherche_gagnee'];
	$data3 = $this->args['recherche_score'];
	if($data1==NULL || $data2==NULL || $data3==NULL){echo "<p>Pas encore de classements disponibles</p>";}
	else{
		echo '
		<div class= col-md-4>';
		echo "
			<h2>Parties jouées</h2>";
		
		echo '
			<div class="well well-lg">';
		echo '
				<table class="table">';
		Classement::classement_toHtml($data1,1,1,0,0);
		echo '
				</table>';
		echo '
			</div>';
		echo '
		</div>';
		
		echo '
		<div class= col-md-4>';
		echo "
			<h2>Victoires</h2>";
		
		echo '
			<div class="well well-lg">';
		echo '
				<table class="table">';
		Classement::classement_toHtml($data2,1,0,1,0);
		echo '
				</table>';
		echo '
			</div>';
		echo '
		</div>';
		
		echo '
		<div class= col-md-4>';
		echo "
			<h2>Meilleurs scores</h2>";
			
		echo '
			<div class="well well-lg">';
		echo '
				<table class="table">';
		Classement::classement_toHtml($data3,1,0,0,1);
		echo '
				</table>';
		echo '
			</div>';
		echo '
		</div>';
	}
?>
</div>
</section>
