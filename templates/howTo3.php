<section>
<div class=container>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Règles du jeu <small>3/4</small></h3>
  </div>
  <div class="panel-body">
    <p class="game-rules"><img  alt="Overview of a game" src="http://localhost/assets/rules-3.png"></p>
    <p>Les cartes sont posées afin de compléter les
    rangées selon les critères suivants :</p>
    <ul><li>la carte doit être jouée sur une rangée dont la valeur de la dernière carte est inférieure à celle de la carte jouée ;</li>
    <li>entre les rangées dont la dernière carte est inférieure à la carte jouée, il faut choisir celle où l'écart avec la carte jouée est le plus faible ;</li>
    <li>si la carte à jouer est de valeur inférieure à la dernière carte de toutes les rangées, le joueur ramasse la rangée valant le moins de points et pose sa carte comme première carte de la nouvelle rangée.
    </li></ul></p>
    <p>Si un joueur ajoute une sixème carte à une rangée, il ramasse les 5 cartes présentes, et la sixième devient la première carte de la rangée. Les points des cartes sont alors ajoutés à son score, qui doit rester le plus faible possible.</p>
    <ul class="pager">
      <?php
      if (isset($this->args['button'])) {
        echo $this->args['button'];
      } ?>
  </ul>
  </div>
</div>
</div>
</section>
