<section>
  <div class="container">
    <hr>
    <?php
    if( !isset($this->args['new_round_required'])){
      if (isset($this->args['hand'])) {
        $hand = $this->args['hand'];
        for ($i=0; $i < count($hand) ; $i++) {
          $card = $hand[$i];
          if (isset($this->args['waiting_for_others'])) {
            echo '<div class="rules-well col-sm-2"><div class="card-'.$card['value'].'-pt card">'.$card['card'].'</div></div>';
          } else if($card['value']==0){
            echo '<div class="rules-well col-sm-2"><div class="card-0-pt card"></div></div>';
          } else{
            echo '<div class="rules-well col-sm-2"><a href="index.php?action=playWithCard&card_id='.$card['card'].'&game_id='.$this->args['game_id'].'"><div class="card-'.$card['value'].'-pt card">'.$card['card'].'</div></a></div>';
          }
        }
      }
    }
    ?>
  </section>
