<section>
  <div class="container">


  <h1>Classement</h1>

  <h1>Recherche</h1>
  <form action="index.php?action=classement" method="post">
      <div class="form-group">
        <label for="Login">Login recherché</label>
        <input type="text" name="Login" id="Login" class="form-control" placeholder="Login"/>
      </div>
      <div class="form-group">
        <label for="Order">Afficher les joueurs en fonction de&#8230;</label>
        <Select name="Order" size="1" id="Order" class="form-control" placeholder="Order"/>
		<option>Toutes les statistiques<option>Parties jouees<option>Parties gagnees<option>Scores
		</select>
      </div>
	   <div class="form-group">
        <label for="Order">Page (int)</label>
        <input type="text" name="Page" id="Page" class="form-control" placeholder="Page"/>
      </div>
      <div class="form-group">
        <button class="btn btn-primary" type="submit" value="classement" name="action">Recherche</button>
    </div>
    </form>

<div class="well well-lg">

<?php
	if(isset($this->args['recherche_pseudo'])){
		if($this->args['recherche_pseudo']==NULL){echo "<p>Pseudo inexistant</p>";}
		else{
			echo '	<table class="table">';
			Classement::recherche_toHtml($this->args['recherche_pseudo']);
			echo '
		</table>';
		}
	}
	elseif(isset($this->args['recherche'])){
		if($this->args['recherche']==NULL){echo "<p>Pas encore assez de classements disponibles</p>";}
		else{
			$data = $this->args['recherche'];
			if($data==0){}
			else{
				echo '<table class="table">';
				switch($this->args['selection']){
					case 0 : Classement::classement_toHtml($data,1,1,1,1);break;
					case 1 : Classement::classement_toHtml($data,0,1,1,1);break;
					case 2 : Classement::classement_toHtml($data,1,1,0,0);break;
					case 3 : Classement::classement_toHtml($data,1,0,1,0);break;
					case 4 : Classement::classement_toHtml($data,1,0,0,1);break;
				}
				echo '</table>';
			}
		}
	}
?>
</div>
</div>

</section>
