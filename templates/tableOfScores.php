<section>
<div class="container">
<?php
if (isset($this->args['message'])) {
  echo $this->args['message'];
}

if (isset($this->args['new_round_required'])) {
  if (isset($this->args['player_is_the_host'])) {
    echo '<a class="btn btn-primary btn-lg" href="index.php?action=buildingNewGame&game_id='.$this->args['game_id'].'" role="button">Lancer le tour suivant</a><br>';;
  } else{
    echo '<a class="btn btn-default btn-lg" href="index.php?action=checkingBoardContent&game_id='.$this->args['game_id'].'" role="button">Actualiser</a><br>';;
  }
}
if (isset($this->args['waiting_for_others'])) {
  echo '<a class="btn btn-default btn-lg" href="index.php?action=checkingBoardContent&game_id='.$this->args['game_id'].'" role="button">Actualiser</a><br>';;
}
if (isset($this->args['scores_in_html'])) {
  echo $this->args['scores_in_html'];
}
 ?>
</div>
</section>
