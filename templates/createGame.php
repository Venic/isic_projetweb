<section>
  <div class="central_layer authenticate">
    <p class="logo-6" id="logo-6-inscription"><img  alt="Logo 6" src="http://localhost/assets/M_6.png"></p>
    <h1>Créer une partie</h1>
<?php
  if (isset($this->args['message'])) {
    echo $this->args['message'];
  }
 ?>
    <p>Merci de remplir tous les champs.</p>

    <form action="index.php?action=launchGame" method="post">
      <div class="form-group">
        <label for="gameName">Nom de la partie</label>
        <input type="text" name="game_name" id="game_name" class="form-control" placeholder="Saisissez ici le nom de votre partie"/>
      </div>
      <div class="form-group">
        <label for="gameSetting">C'est une partie&#8230;</label>
        <div class="radio">
          <label>
            <input type="radio" name="gameSetting" id="publicGame" checked value="1">
            publique<br><em>accessible depuis l'accueil de tous les utilisateurs.</em>
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="gameSetting" id="privateGame" value="0">
            privée<br><em>accessible aux utilisateurs qui auront reçu une invitation.</em>
          </label>
        </div>
      </div>
      <div class="form-group">
        <button class="btn btn-primary" type="submit" value="launchGame" name="action">Créer la partie</button>
    </div>
    </form>
  </div>
</section>
