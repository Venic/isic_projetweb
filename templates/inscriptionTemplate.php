<section>
  <div class="central_layer authenticate">
    <p class="logo-6" id="logo-6-inscription"><img  alt="Logo 6" src="http://localhost/assets/M_6.png"></p>
    <h1>Inscription</h1>
    <p>Pour jouer, remplissez tous les champs.</p>
    <?php
    if (isset($this->args['message'])) {
      echo $this->args['message'];
    }
    ?>

    <form action="index.php" method="post">
      <div class="form-group">
        <label for="inscLogin">Login</label>
        <input type="text" name="inscLogin" id="inscLogin" class="form-control" placeholder="Votre login"/>
      </div>
      <div class="form-group">
        <label for="inscPassword">Mot de passe</label>
        <input type="password" id="inscPassword" name="inscPassword"  class="form-control" placeholder="Mot de passe"/>
      </div>
      <div class="form-group">
        <label for="confirmPassword">Saisissez à nouveau votre mot de passe</label>
        <input type="password" id="confirmPassword" name="confirmPassword"  class="form-control" placeholder="Mot de passe"/>
      </div>
      <div class="form-group">
        <button class="btn btn-primary" type="submit" value="validationInscription" name="action">Créer le compte</button>
    </div>
    </form>
    <p><a href="index.php?action=defaultAction">Déjà un compte ? Connectez-vous</a></p>
  </div>
</section>
