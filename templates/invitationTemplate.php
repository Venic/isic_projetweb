<section>
  <div class="central_layer authenticate">
    <p class="logo-6" id="logo-6-inscription"><img  alt="Logo 6" src="http://localhost/assets/M_6.png"></p>
    <h1>Inviter un joueur</h1>
    <?php
    if(isset($this->args['game_name'])){
      echo 'Invitez un ami à rejoindre votre partie '.$this->args['game_name'].'.<br> Saisssez en dessous son nom.';
    }
    if (isset($this->args['message'])) {
      echo $this->args['message'];
    }
    echo '<form action="index.php?game_id='.$this->args['game_id'].'" method="post">';
    ?>
    <div class="form-group">
      <label for="guestLogin">Login</label>
      <input type="text" name="guestLogin" id="guestLogin" class="form-control" placeholder="Login de l'invité"/>
    </div>  <div class="form-group">
      <button class="btn btn-primary" type="submit" value="inviteAnotherUser" name="action">Envoyer l'invitation</button>
    </div></form>
  </div>
</section>
