<section>
<div class=container>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Règles du jeu <small>2/4</small></h3>
  </div>
  <div class="panel-body">
    <p class="game-rules"><img  alt="Overview of a game" src="http://localhost/assets/rules-2.png"></p>
    <p>À chaque tour, cliquez sur la carte que vous voulez jouer. <em>Elle n'apparaîtra pas immédiatement sur le plateau :
      nous devons attendre que tous vos adversaires aient joué pour la poser</em>. Les cartes sont ensuite jouées par ordre croissant.</p>
    <ul class="pager">
      <?php
      if (isset($this->args['button'])) {
        echo $this->args['button'];
      } ?>
  </ul>
  </div>
</div>
</div>
</section>
