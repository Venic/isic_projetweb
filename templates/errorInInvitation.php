<section>
  <div class="central_layer authenticate">
    <p class="logo-6" id="logo-6-inscription"><img  alt="Logo 6" src="http://localhost/assets/M_6.png"></p>
    <h1>Inviter un joueur</h1>
    <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Pas d'identifiant correspondant à votre saisie. <br>
    Vous pouvez choisir dans la liste ci-dessous.</div>
      <?php
      if (isset($this->args['error_in_pseudo'])) {
        echo '<table class="table table-hover"><tr><th>Pseudonyme</th></tr>';
        $currentPseudo = $this->args['table_of_pseudos'];
        for ($i=0; $i < count($this->args['table_of_pseudos']); $i++) {
          if (strcmp($this->args['pseudo'],$currentPseudo[$i])!=0) {
            echo '<tr><td><a href="index.php?action=inviteAnotherUser&guestLogin='.$currentPseudo[$i].'&game_id='.$this->args['game_id'].'">'.$currentPseudo[$i].'</a></td></tr>';
          }
        }
        echo '</table>';
      }
       ?>
  </div>
</section>
