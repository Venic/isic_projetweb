<section>
  <div class="container">
    <?php
    if(isset($this->args['game_name'])){
      echo '<h1>Résultats de la partie '.$this->args['game_name'].'</h1>';
    }
    if (isset($this->args['list_scores']) && isset($this->args['player_id'])) {
      $playerList =$this->args['list_scores'];
      echo '<div class="well-board well well-lg">';
      echo '<table class="table">
      <tr>
        <th>Pseudonyme</th>
        <th>Score</th>
      </tr>';
      for ($i=0; $i < count($playerList); $i++) {
        echo "<tr>";
        $gameTMP = $playerList[$i];
        echo "<td>".$gameTMP['pseudo']."</td>";
        echo "<td>".$gameTMP['score']."</td>";
        echo "</tr>";
      }
      echo"</table></div>";
      echo '<a class="btn btn-primary btn-lg" href="index.php?action=updatingGameStatus&player_id='.$this->args['player_id'].'&game_id='.$this->args['game_id'].'" role="button">Retour à l\'accueil</a><br>';
    } else {
      echo '<div class="alert alert-success" role="alert">La partie que vous essayez de consulter n\'existe plus.</div>';
      echo '<a class="btn btn-primary btn-lg" href="index.php?action=defaultAction" role="button">Retour à l\'accueil</a><br>';
    }
     ?>
  </div>
</section>
