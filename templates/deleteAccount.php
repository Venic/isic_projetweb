<section>
<div class="container">

  <div class="central_layer authenticate">
    <p class="logo-6"><img  alt="Logo 6" src="http://localhost/assets/M_6.png"></p>
    <h1>Suppression de compte</h1>
    <?php
    if (isset($this->args['message'])) {
      echo $this->args['message'];
    }
    ?>
    <p>Souhaitez-vous réellement effacer votre compte ?<br> Vous perdrez définitivement toutes les statistiques qui vous sont liées.</p>
    <form action="index.php" method="get">
      <div class="form-group">
        <button class="btn btn-danger" type="submit" value="confirmDeletion" name="action">Supprimer</button>
      </div>
      <div class="form-group">
        <button class="btn btn-default" type="submit" value="defaultAction" name="action">Annuler </button>
      </div>

    </form>
  </div>
</section>
