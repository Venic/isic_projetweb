<section>
<div class=container>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Règles du jeu <small>4/4</small></h3>
  </div>
  <div class="panel-body">
    <p>Les cartes, en plus d'avoir une valeur faciale, ont une valeur assignée en points. Attention, ce sont ces points qui ont une incidence sur le cours de la partie. Faites tout pour en avoir le moins possible !</p>
    <ul><li>La carte 55 vaut 7 points</li>
    <li>Les cartes multiples de 11 (11, 22, 33 jusqu'à 99 inclus) valent 5 points</li>
    <li>Les cartes multiples de 10 (10, 20, 30 jusqu'à 100 inclus) valent 3 points </li>
    <li>Les cartes ayant 5 au chiffre des unités (5, 15, 25 jusqu'à 95 inclus) valent 2 points </li>
    <li>Les autres cartes valent un point.</li>
    </ul>
    <div class="well well-lg" id="well-rules">
      <div class="rules-well col-md-2"><div class="card-1-pt card">18</div><br>1 point</div>
     <div class="rules-well col-md-2"><div class="card-2-pt card">25</div><br>2 points</div>
     <div class="rules-well col-md-2"><div class="card-3-pt card">30</div><br>3 points</div>
     <div class="rules-well col-md-2"><div class="card-5-pt card">44</div><br>5 points</div>
     <div class="rules-well col-md-1"><div class="card-7-pt card">55</div><br>7 points</div>
    </div>

    <p><em>Le jeu s'arrête lorsqu'un joueur atteint le seuil de 66 points. Bon courage !</em></p>
    <ul class="pager">
      <?php
      if (isset($this->args['button'])) {
        echo $this->args['button'];
      } ?>
  </ul>
  </div>
</div>
</div>
</section>
