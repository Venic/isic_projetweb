<section>
<div class=container>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Règles du jeu <small>1/4</small></h3>
  </div>
  <div class="panel-body">
    <p class="game-rules"><img  alt="Overview of a game" src="http://localhost/assets/rules-1.png"></p>
    <p>Merci de jouer au Six qui ramasse ! Découvrons les règles du jeu. Vous avez à l'écran deux zones :</p>
      <ul>
        <li>Le plateau, avec 4 rangées, à lire horizontalement, visible par tous.</li>
        <li>En bas de l'écran, vos cartes, visibles par vous seul.</li>
      </ul>
    <p>Au début du jeu, une carte est posée au début de chaque rangée et chaque joueur reçoit 10 cartes.</p>

    <ul class="pager">
      <?php
      if (isset($this->args['button'])) {
        echo $this->args['button'];
      } ?>
      </ul>
  </div>
</div>
</div>
</section>
