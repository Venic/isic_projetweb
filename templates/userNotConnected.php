<section>
  <div class="central_layer authenticate">
    <p class="logo-6"><img  alt="Logo 6" src="http://localhost/assets/M_6.png"></p>
    <h1>Bienvenue !</h1>
    <p>Vous avez déjà un compte ? <br>Authentifiez-vous avec votre pseudo et mot de passe.</p>
    <?php
    if (isset($this->args['message'])) {
      echo $this->args['message'];
      }
    ?>
    <form action="index.php?action=validationConnexion" method="post">
      <div class="form-group">
        <?php
        if (isset($this->args['pseudo'])){
          echo '<input type="text" name="inscLogin" class="form-control" placeholder="Pseudo" value="'.$this->args['pseudo'].'"/><br/>';
        } else{
          echo '<input type="text" name="inscLogin" class="form-control" placeholder="Pseudo"/><br/>';
        }
         ?>
        <input type="password" name="inscPassword"  class="form-control"  placeholder="Mot de passe"/><br/>
        <button class="btn btn-primary" type="submit" value="validationConnexion" name="action">Connexion</button>
      </div>
    </form>
    <hr>
    <p>Pas encore de compte ? Créez le votre en deux clics !
    <form action="index.php?action=inscription" method="post">
      <input type="submit" class="btn btn-default" value="Créer un compte"/>
    </form>
	<p><a href="index.php?action=readRules&counter=1">Règles et classements</a></p>
  </div>
</section>
