<section>
  <div class="container">
    <?php
    if (isset($this->args['sound'])) {
      echo '<audio src="'.$this->args['sound'].'" autoplay>  Your browser does not support the <code>audio</code> element.</audio>';
    }
    if (isset($this->args['message'])) {
      echo $this->args['message'];
    }
    if (isset($this->args['greetings'])){
      echo '<h2>'.$this->args['greetings'].'</h2>';
    }
    if (isset($this->args['gameList']) ||isset($this->args['gameInvolved']) ) {
        echo '<h1>Parties disponibles</h1> <div class="well well-lg" id="well-rules">';
        if (isset($this->args['gameInvolved']) && count($this->args['gameInvolved'])>0) {
          echo'  <table class="table"> <tr>
          <th>Nom de la partie</th>
          <th>Partie publique/privée</th>
          <th>Le jeu est démarré</th>
          <th>Vous êtes invité</th>
          <th>Rejoindre</th>
          </tr>';
          $gameList = $this->args['gameInvolved'];
          for ($i=0; $i < count($gameList); $i++) {
            echo "<tr>";
            $gameTMP = $gameList[$i];
            echo "<td>".$gameTMP['game_name']."</td>";
            if ($gameTMP['public_private']==1) {
              echo '<td><span class="glyphicon glyphicon-globe" aria-hidden="true" ></span></td>';
            } else{
              echo '<td><span class="glyphicon glyphicon-lock" aria-hidden="true" ></span></td>';
            }
            // echo "<td>".$gameTMP['public_private']."</td>";
            if ($gameTMP['on_going']==1) {
              echo '<td><span class="glyphicon glyphicon-ok" aria-hidden="true" ></span></td>';
            } else{
              echo '<td><span class="glyphicon glyphicon-minus" aria-hidden="true" ></span></td>';
            }            // if ($gameTMP['is_host']==1) {
            //   echo '<td><span class="glyphicon glyphicon-home" aria-hidden="true" ></span></td>';
            // } else{
            //   echo '<td><span class="glyphicon glyphicon-minus" aria-hidden="true" ></span></td>';
            // }
            if ($gameTMP['is_guest']==1) {
              echo '<td><span class="glyphicon glyphicon-ok" aria-hidden="true" ></span></td>';
            } else{
              echo '<td><span class="glyphicon glyphicon-minus" aria-hidden="true" ></span></td>';
            }
            // echo "<td>".$gameTMP['is_guest']."</td>";
            echo '<td><a class="btn btn-primary" href="index.php?action=waitingRoom&game_id='.$gameTMP['game_id'].'&game_name='.$gameTMP['game_name'].'">Rejoindre</a></td>';
            echo "</tr>";
          }
          echo"</table>";
        } else if (isset($this->args['gameList'])) {
          $gameList = $this->args['gameList'];
          echo' <table class="table"> <tr>
          <th>Nom de la partie</th>
          <th>Nombre de joueurs</th>
          <th>Rejoindre</th>
          </tr>';
          for ($i=0; $i < count($gameList); $i++) {
            echo "<tr>";
            $gameTMP = $gameList[$i];
            echo "<td>".$gameTMP['game_name']."</td>";
            echo "<td>".$gameTMP['counter']."</td>";
            echo '<td><a class="btn btn-primary" href="index.php?action=waitingRoom&game_id='.$gameTMP['game_id'].'&game_name='.$gameTMP['game_name'].'">Rejoindre</a></td>';
            echo "</tr>";
          }
          echo "</table>";
        }
        if (isset($this->args['button'])) {
          echo $this->args['button'];
        }
      echo "</div>";
      }
      ?>
  </div>
</section>
