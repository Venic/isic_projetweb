<section>
  <div class="headline">
    <h1>Parties publiques en attente</h1>
    <div class="central_layer box-homepage-user">
      <table class="table table-hover">
        <tr>
          <th>Nom de l'hôte</th>
          <th>Joueurs prévus</th>
          <th>Joueurs inscrits</th>
          <th>Rejoindre</th>
        </tr>
        <tr>
          <td>Romain</td>
          <td>12</td>
          <td>45</td>
          <td><a class="btn-sm btn-info" href="#" role="button">Learn more</a></td>
        </tr>
        <tr>
          <td>Clément</td>
          <td>123</td>
          <td>123</td>
          <td><a class="btn-sm btn-info" href="#" role="button">Learn more</a></td>
        </tr>
        <!-- Ne pas mettre trop de rangs -->
      </table>
      <a class="btn-sm btn-default  glyphicon glyphicon-menu-left" href="#" role="button"></a>
      <a class="btn-sm btn-default  glyphicon glyphicon-menu-right" href="#" role="button"></a>

    </div>
  </div>

</section>
