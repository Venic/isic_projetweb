<section>
  <div class="container">
    <?php
    if (isset($this->args['game_name'])) {
      echo "<h1>".$this->args['game_name']."</h1>";
    } else{
      echo "<h1>Erreur</h1>";
    }
    if (isset($this->args['message'])) {
      echo $this->args['message'];
    }
    if (isset($this->args['game_players'])) {
      if (count($this->args['game_players'])>0) {
        $infos_players = $this->args['game_players'];
        echo '<div class="well well-lg" id="well-rules"><table class="table">';
        echo "<tr><th>Nom du joueur</th><th>Prêt ?</th></tr>";
        for ($i=0; $i < count($infos_players); $i++) {
          $players_loop = $infos_players[$i];
          echo "<tr>";
          echo "<td>".$players_loop['login']."</td>";
          if ($players_loop['ready']==1) {
            echo '<td><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>';
          } else{
            echo '<td><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></td>';
          }
          echo "</tr>";
        }
        echo "</table></div>";
      }
    }
    if (isset($this->args['player_ready'])) {
      if ($this->args['player_ready']==0) {
        echo '<a class="btn btn-primary btn-lg" href="index.php?action=playerIsReady&game_name='.$this->args['game_name'].'&game_id='.$this->args['game_id'].'" role="button">Je suis prêt!</a>';
      }  else if ($this->args['nb_ready']==count($infos_players) && count($infos_players)>1){
        if (isset($this->args['game_launched'])) {
          echo  '<a class="btn btn-success btn-lg" href="index.php?action=buildingLaunchedGame&game_id='.$this->args['game_id'].'" role="button">Démarrer le jeu</a>';
        } else{
          echo  '<a class="btn btn-success btn-lg" href="index.php?action=buildingnewGame&game_id='.$this->args['game_id'].'" role="button">Démarrer le jeu</a>';
        }
      } else {
        echo '<a class="btn btn-primary btn-lg" href="index.php?action=waitingRoom&game_name='.$this->args['game_name'].'&game_id='.$this->args['game_id'].'" role="button">Actualiser</a>';
      } if (!isset($this->args['hide_invitation'])) {
        echo ' <a class="btn btn-default" href="index.php?action=invitePlayers&game_id='.$this->args['game_id'].'" role="button">Inviter</a>';
      }
      echo  ' <a class="btn btn-danger" href="index.php?action=deleteGameSubscription&game_id='.$this->args['game_id'].'" role="button">Quitter le jeu</a>';
    }
    ?>
  </div>

</section>
