<section>
  <div class="container">
    <h1>Statistiques</h1>
    <?php
    if (isset($this->args['messageTwo'])) {
      echo $this->args['messageTwo'];
    }
    if (isset($this->args['playerStats'])) {
      echo '
      <div class="well well-lg" id="well-rules">
      <table class="table">';
      $tmp = $this->args['playerStats'];
      echo "<tr><th>Nombre de parties jouées</th>";
      echo "<td>".$tmp['played_games']."</td></tr>";
      echo "<tr><th>Nombre de parties gagnées</th>";
      echo "<td>".$tmp['games_won']."</td></tr>";
      echo "<tr><th>Score moyen</th>";
      echo "<td>".$tmp['average_score']."</td></tr>";
    }
    echo '</table></div>';
    ?>
  </div>

</section>
