<?php
/**
* List of all functions used in this class
* Name of functions                             Type of element returned        Purpose of the function
* insertCardInStack                             Stack / null                    Insert a card in the player's Stack.
* deleteWholeStack                              Stack / null                    Deletes all the stacks related to a game.
*/
class Stack extends MyObject
{

  function __construct()
  {
    # code...
  }

  public static function insertCardInStack($index_of_game,$index_of_player,$index_of_card){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'INSERT INTO recoit (NUMERO_CARTE, ID_PARTIE, ID_JOUEUR) VALUES ('.$index_of_card.','.$index_of_game.','.$index_of_player.')';
      $result = $dbPDO->query($query);
      return new Stack;
    } catch (PDOException $e) {
      echo "Could not insert card in stack";
    }
  }

  public static function deleteWholeStack($index_of_game){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'DELETE FROM recoit WHERE ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($query);
      return new Stack;
    } catch (PDOException $e) {
      echo "Could not clean stack! ";
    }
    return null;
  }
}
 ?>
