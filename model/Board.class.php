<?php

/**
* List of all functions used in this class
* Name of functions             Type of element returned        Purpose of the function
* checkifGameLaunched           int / null                      Checks and returns the number of cards on the board.
* addCardToBoard                Board                           Add a card to the board
* getCardFromBoard              array                           Fetch all the cards of a game
* deleteCardFromBoard           Board                           Delete a card from the board. Another function writes the card in the player's stack.
* updateCardOnBoard             Board                           Moves the card from row 5, which contains the cards to be played, to the row that has calculated.
* getCardsFromARow              array                           Fetch the card from a row in a particular game
* deleteRowFromBoard            Board                           Deletes the cards on the row
* deleteWholeBoard              Board                           Deletes the cards on the whole board
* getMaximalValueOfRows         array                           Returns the maximal value of card found on each row
* getScoreOfRow                 array                           Return the number of points the player would earn if he decides to take the cards on a row.
*/
class Board extends MyObject
{
  protected

  function __construct()
  {
    # code...
  }

  public static function checkifGameLaunched($index_of_game){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'SELECT COUNT(NUMERO_CARTE) AS COMPTEUR FROM possede WHERE ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($query);
      $data=$result->fetch(PDO::FETCH_OBJ);
      return $data->COMPTEUR;
    } catch (PDOException $e) {
      echo "Could not know if the game was already launched";
    }
    return null;
  }

  public static function addCardToBoard($index_of_game,$index_of_row,$index_of_card){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'INSERT INTO possede VALUES ('.$index_of_card.','.$index_of_game.','.$index_of_row.');';
      $result = $dbPDO->query($query);
      return new Board;
    } catch (PDOException $e) {
      echo "Did not manage to insert the card in possede";
    }
    return null;
  }

  public static function getCardFromBoard($index_of_game){
    try {
      $array_global = array( );
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'SELECT possede.NUMERO_CARTE, possede.NUMERO_RANGEE, carte.VALEUR FROM possede,carte WHERE possede.ID_PARTIE='.$index_of_game.' AND possede.NUMERO_CARTE=carte.NUMERO_CARTE ORDER BY NUMERO_RANGEE ASC;';
      $result = $dbPDO->query($query);
      $data=$result->fetch(PDO::FETCH_OBJ);
      $index_of_row=1;
      while (!empty($data)) {
        $array_row=array();
        $current_row = $data->NUMERO_RANGEE;
        while ($current_row == $index_of_row) {
          $card_number = $data->NUMERO_CARTE;
          $value = $data->VALEUR;
          $array_card=  array('card' => $card_number, 'value'=>$value);
          array_push($array_row,$array_card);
          $data=$result->fetch(PDO::FETCH_OBJ);
          if (!empty($data)) {
            $current_row = $data->NUMERO_RANGEE;
          } else{
            $current_row=0;
          }
        }
        while(count($array_row)<5){
          $array_card= array('card' => 0, 'value'=>0);
          array_push($array_row,$array_card);
        }
        $index_of_row++;
        array_push($array_global,$array_row);
      }
      return $array_global;
    } catch (PDOException $e) {
      echo "Error when fetching the cards on the board.";
    }
    return null;
  }

  public static function deleteCardFromBoard($index_of_game,$index_of_card){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'DELETE FROM possede WHERE NUMERO_CARTE= '.$index_of_card.' AND ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($query);
      $result->execute();
      return new Board;
    } catch (PDOException $e) {
      echo "Did not manage to delete the card in possede";
    }
    return null;
  }

  public static function updateCardOnBoard($index_of_game,$index_of_row,$index_of_card){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'UPDATE possede SET NUMERO_RANGEE= '.$index_of_row.' WHERE ID_PARTIE='.$index_of_game.' AND NUMERO_CARTE='.$index_of_card.';';
      $result = $dbPDO->query($query);
      $result->execute();
      return new Board;
    } catch (PDOException $e) {
      echo "Did not manage to move the card in possede";
    }
    return null;
  }

  public static function getCardsFromARow($index_of_game,$index_of_row){
    try {
      $array_of_cards = array();
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'SELECT NUMERO_CARTE FROM possede WHERE ID_PARTIE='.$index_of_game.' AND NUMERO_RANGEE='.$index_of_row.';';
      $result = $dbPDO->query($query);
      $data=$result->fetch(PDO::FETCH_OBJ);
      while (!empty($data)) {
        array_push($array_of_cards,$data->NUMERO_CARTE);
        $data=$result->fetch(PDO::FETCH_OBJ);

      }
      return $array_of_cards;
    } catch (PDOException $e) {
      echo "Couldn't get cards on the row !";
    }
    return null;
  }

  public static function deleteRowFromBoard($index_of_game,$index_of_row){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'DELETE FROM possede WHERE NUMERO_RANGEE= '.$index_of_row.' AND ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($query);
      return new Board;
    } catch (PDOException $e) {
      echo "Couldn't manage to do the cleanup on the row! ";
    }
    return null;
  }

  public static function deleteWholeBoard($index_of_game){
    try{
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'DELETE FROM possede WHERE ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($query);
      return new Board;
    } catch (PDOException $e){
      echo "Couldn't do the cleanup on the board !";
    }
    return null;
  }

  public static function getMaximalValueOfRows($index_of_game){
    try {
      $array_of_maximas = array();
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'SELECT MAX(NUMERO_CARTE) AS MAXIMUM FROM possede WHERE ID_PARTIE='.$index_of_game.' GROUP BY NUMERO_RANGEE ORDER BY NUMERO_RANGEE ASC LIMIT 0,4;';
      $result = $dbPDO->query($query);
      $data=$result->fetch(PDO::FETCH_OBJ);
      while (!empty($data)) {
        array_push($array_of_maximas,$data->MAXIMUM);
        $data=$result->fetch(PDO::FETCH_OBJ);
      }
      return $array_of_maximas;
    } catch (PDOException $e) {
      echo "Could not get maximas of board";
    }
    return null;
  }

  public static function getScoreOfRow($index_of_game){
    try {
      $array_of_points=array();
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'SELECT SUM(carte.VALEUR) as SOMME FROM carte,possede WHERE carte.NUMERO_CARTE=possede.NUMERO_CARTE AND possede.ID_PARTIE='.$index_of_game.' GROUP BY possede.NUMERO_RANGEE ORDER BY possede.NUMERO_RANGEE LIMIT 0,4;';
      $result = $dbPDO->query($query);
      $data=$result->fetch(PDO::FETCH_OBJ);
      while (!empty($data)) {
        array_push($array_of_points,$data->SOMME);
        $data=$result->fetch(PDO::FETCH_OBJ);
      }
      return $array_of_points;
    } catch (PDOException $e) {

    }

  }
}
?>
