<?php
/**
* List of all functions used in this class
* Name of functions                             Type of element returned        Purpose of the function
* isLoginUsed                                   boolean                         Checks if the login given as a paramater is already used in the dataBase
* createPlayer                                  User                            Creates the account.
* isThereAnUserWithThatNickname                 int / null                      Checks if there is a user with that nicknames
* gettingNickNamesThatBeginsWith                array / null                    Gets all the nicknames that begin with a particular letter.
* checkIfLoginPresent                           User / null                     Checks if a username and an appropriate login exist in the database.
* delete                                        boolean                         Deletes an account.
* changePassword                                boolean                         Changes a password.
* getStats                                      array / null                    Get all the stats related to a player.
* updatesGamesPlayed                            User / null                     Adds one to the number of games played.
* updateGamesWon                                User / null                     Adds one to the number of games won.
* updateAverageScore                            User / null                     Updates the average score. Must be called AFTER a change to the number of games played has been done.
* getNumberOfGamesPlayed                        int / null                      Gets the number of games played. Useful if the game rules have to be displayed.
*/
class User extends MyObject
{

  function __construct()
  {
    # code...
  }

  public static function isLoginUsed($login){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT COUNT(*) AS compteur FROM compte WHERE compte.PSEUDO = "'.$login.'";';
      //echo $request;
      $result = $dbPDO->query($request);
      //if ($result) {
      $data = $result->fetch(PDO::FETCH_OBJ);
      //}
      $loginAlreadyUsed = 0;
      while (!empty($data)) {
        if ($data->compteur != 0) {
          $loginAlreadyUsed = $data->compteur;
          return true;
        }
        $data = $result->fetch(PDO::FETCH_OBJ);
      }
    } catch (PDOException $error) {
      echo '456 Failed to connect to MySQL.';
    }
    return false;

  }

  public static function create($login,$password){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = $dbPDO->prepare('INSERT INTO compte VALUES(:pseudo,:password,0,0,NULL,0,NULL)');
      $request->bindParam(':pseudo',$login);
      $request->bindParam(':password',$password);
      $request->execute();
      return new User;
      } catch (PDOException $error) {
        echo "ERREUR insertion";
      }
  }

  public static function isThereAnUserWithThatNickname($login){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request='SELECT COUNT(*) AS COMPTEUR FROM compte WHERE  compte.PSEUDO="'.$login.'";';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      return $data->COMPTEUR;
    } catch (PDOException $e) {
      echo "Couldn't find a user with such a nickname";
    }
    return null;

  }

  public static function gettingNickNamesThatBeginsWith($letter){
    try {
      $array_of_nicknames=array();
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT PSEUDO FROM compte WHERE PSEUDO LIKE "'.$letter.'%";';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      while (!empty($data)) {
        array_push($array_of_nicknames,$data->PSEUDO);
        $data = $result->fetch(PDO::FETCH_OBJ);
      }
      return $array_of_nicknames;
    } catch (PDOException $e) {
      echo "Could not get the list of nicknames that begin with the same letter!";
    }
    return null;
  }

  public static function checkifLoginPresent($login,$password){
    try{
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = "SELECT COUNT(*) AS compteur FROM compte WHERE compte.PSEUDO ='".$login."' AND compte.MOT_DE_PASSE='".$password."';'";
      //echo $request;
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      while (!empty($data)) {
      //  echo $data->compteur;
        if ($data->compteur == 1) {
          $loginAndPasswordFound=1;
          return new User;
        }
        $data = $result->fetch(PDO::FETCH_OBJ);
      }
    }catch(PDOException $error){
      echo 'Could not connect to MySQL.';
    }
  }

  public static function delete($login){
    try{
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'DELETE FROM compte WHERE compte.PSEUDO ="'.$login.'"';
      //$request->bindParam(':pseudo',$login);
      $result = $dbPDO->query($request);
      $result->execute();
      return true;
    }catch(PDOException $error){
      echo "Problème à la suppression du compte";
      return false;
    }
  }

  public static function changePassword($login,$newPassword,$oldPassword){
    if(!is_null(User::checkIfLoginPresent($login,$oldPassword))){
      try{
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'UPDATE compte SET MOT_DE_PASSE="'.$newPassword.'" WHERE PSEUDO="'.$login.'"';
    //  $request->bindParam(':pseudo',$login);
    //  $request->bindParam(':password',$newPassword);
    $result = $dbPDO->query($request);
//$data = $result->fetch(PDO::FETCH_OBJ);
      $result->execute();
      return true;
    }catch(PDOException $error){
      echo "Problème au changement de mot de passe";
    }
  }
    return false;
  }

  public static function getStats($login){
    $array = null;
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT PARTIE_JOUEE,PARTIE_GAGNEE,SCORE_MOYEN FROM compte WHERE PSEUDO="'.$login.'"';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      if (!empty($data)){
        $array = array('played_games'=>$data->PARTIE_JOUEE, 'games_won'=>$data->PARTIE_GAGNEE, 'average_score'=>$data->SCORE_MOYEN);
        return $array;
      }
    } catch (PDOException $e) {
      echo "Failed to get stats";
    }
    return null;
  }

  public static function updateGamesPlayed($login){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'UPDATE compte SET PARTIE_JOUEE=PARTIE_JOUEE+1 WHERE PSEUDO="'.$login.'";';
      $result = $dbPDO->query($request);
      return new User;
    } catch (PDOException $e) {
      echo "Couldn't update the number of games played!";
    }
    return null;
  }

  public static function updateGamesWon($login){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'UPDATE compte SET PARTIE_GAGNEE=PARTIE_GAGNEE+1 WHERE PSEUDO="'.$login.'";';
      $result = $dbPDO->query($request);
      return new User;
    } catch (PDOException $e) {
      echo "Couldn't update the number of games won!";
    }
    return null;
  }

  public static function updateAverageScore($login,$new_score){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'UPDATE compte SET SCORE_MOYEN= (('.$new_score.'+(SCORE_MOYEN*(PARTIE_JOUEE-1)))/PARTIE_JOUEE) WHERE PSEUDO="'.$login.'" AND NOT SCORE_MOYEN IS NULL; ' 
		.'UPDATE compte SET SCORE_MOYEN= '.$new_score.' WHERE PSEUDO="'.$login.'" AND SCORE_MOYEN IS NULL;';
      echo $request;
      $result = $dbPDO->query($request);
      return new User;
    } catch (PDOException $e) {
      echo "Couldn't update the average score !";
    }
    return null;
  }

  public static function getNumberOfGamesPlayed($login){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT PARTIE_JOUEE FROM compte WHERE PSEUDO="'.$login.'"';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      if (!empty($data)) {
        return $data->PARTIE_JOUEE;
      }
    } catch (PDOException $e) {
      echo "Couldn't get the number of games played !";
    }
    return null;
  }

}
 ?>
