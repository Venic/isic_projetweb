<?php
/**
* List of all functions used in this class
* Name of functions                             Type of element returned        Purpose of the function
* enterGameInDatabase                           boolean                         inserts the game in the Database.
* deleteGame                                    Game / null                     deletes the game from the database.
* getGameIdOfGameJustCreated                    int / null                      gets the id of the latest created game.
* changeGameState                               Game / null                     switches the state of the game : in progress or not.
* getGameName                                   int / null                      gets the name of the game defined by its id.
* fetchAvailableGames                           array / null                    gets all the available public games that are not currently being played.
 */
class Game extends MyObject
{

  protected $id_game;

  function __construct()
  {

  }

public static function enterGameInDatabase($name_of_game,$private_public){
  try {
    $dbPDO = DatabasePDO::getUniqueDataBase();
    $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $request = 'INSERT INTO partie(NOM_PARTIE,PUBLIQUE,EN_COURS) VALUES ("'.$name_of_game.'",'.$private_public.',0)';
    $result = $dbPDO->query($request);
    return true;
  } catch (PDOException $e){
    echo "Failure when creating game";
  }
  return false;
}

public static function deleteGame($index_of_game){
  try {
    $dbPDO = DatabasePDO::getUniqueDataBase();
    $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $request = 'DELETE FROM partie WHERE ID_PARTIE='.$index_of_game.';';
    $result = $dbPDO->query($request);
    return new Game;
  } catch (PDOException $e) {
    echo "Couldn't delete the game!";
  }
  return null;
}

public static function getGameIdOfGameJustCreated(){
  try {
    $dbPDO = DatabasePDO::getUniqueDataBase();
    $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $request = 'SELECT ID_PARTIE FROM partie ORDER BY ID_PARTIE DESC LIMIT 0,1;';
    $result = $dbPDO->query($request);
    $data = $result->fetch(PDO::FETCH_OBJ);
    return $data->ID_PARTIE;
  } catch (PDOException $e) {
    echo "Could not get game's id.";
  }
  return null;
}

public static function changeGameState($game_id,$new_state){
  try {
    $dbPDO = DatabasePDO::getUniqueDataBase();
    $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = 'UPDATE partie SET EN_COURS='.$new_state.' WHERE ID_PARTIE='.$game_id.';';
    $result = $dbPDO->query($query);
    return new Game;
  } catch (PDOException $e) {
    echo "Could not manage to change state of the current game";
  }
  return null;
}

public static function getGameName($index_of_game){
  try {
    $dbPDO = DatabasePDO::getUniqueDataBase();
    $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = 'SELECT NOM_PARTIE FROM partie WHERE ID_PARTIE='.$index_of_game.';';
    $result = $dbPDO->query($query);
    $data = $result->fetch(PDO::FETCH_OBJ);
    if (!empty($data)) {
      return $data->NOM_PARTIE;
    }
  } catch (PDOException $e) {
    echo "Could not get the name of the current game";
  }
  return null;
}

public static function fetchAvailableGames(){
  $array_of_games =  array();
  try {
    $dbPDO = DatabasePDO::getUniqueDataBase();
    $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $request = 'SELECT partie.ID_PARTIE, partie.NOM_PARTIE, partie.EN_COURS, partie.PUBLIQUE, COUNT(joueur.PSEUDO) AS somme FROM partie JOIN joueur ON partie.ID_PARTIE=joueur.ID_PARTIE WHERE (partie.PUBLIQUE=1 AND partie.EN_COURS=0 /*AND somme < 10*/ ) GROUP BY joueur.ID_PARTIE ORDER BY joueur.ID_PARTIE ASC';
    $result = $dbPDO->query($request);
    $data = $result->fetch(PDO::FETCH_OBJ);
    while (!empty($data)) {
      $array_loop = array('game_name' => $data->NOM_PARTIE, 'counter' => $data->somme, 'game_id' => $data->ID_PARTIE);
      if ($data->somme <10) {
        array_push($array_of_games,$array_loop);
      }
      $data = $result->fetch(PDO::FETCH_OBJ);
    }
    return $array_of_games;
  } catch (PDOException $e) {
    echo "Failed to get available games";
  }
  return NULL;
}

}
 ?>
