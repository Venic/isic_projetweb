<?php

/**
* List of all functions used in this class
* Name of functions                             Type of element returned        Purpose of the function
* insertCardInHand                              Hand / null                     
 */
class Hand extends MyObject
{

  protected function __construct()
  {
  }

  public static function insertCardInHand($index_of_game,$index_of_card,$index_of_player){
    //on insère la carte dans la BDD
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'INSERT INTO gere VALUES ('.$index_of_card.','.$index_of_game.','.$index_of_player.')';
      // $result = $dbPDO -> prepare('INSERT INTO gere VALUES (:card_id,:game_id,:player_id)');
      // $result->bindParam(':game_id',$index_of_game);
      // $result->bindParam(':card_id',$index_of_card);
      // $result->bindParam(':player_id',$index_of_player);
      $result = $dbPDO->query($query);
    //  $result->execute();
      return new Hand;
    } catch (PDOException $e) {
      echo "Did not manage to insert the card in gere";
    }
    return null;
  }

  public static function getCardFromHand($index_of_game,$index_of_player){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'SELECT gere.NUMERO_CARTE,carte.VALEUR FROM gere,carte WHERE ID_PARTIE='.$index_of_game.' AND ID_JOUEUR='.$index_of_player.' AND carte.NUMERO_CARTE=gere.NUMERO_CARTE;';
      $result = $dbPDO->query($query);
//      $result->execute();
      $data = $result->fetch(PDO::FETCH_OBJ);
      $array_of_cards=array();
      while (!empty($data)) {
        $array_card=  array('card' => $data->NUMERO_CARTE, 'value'=>$data->VALEUR);
        array_push($array_of_cards,$array_card);
        $data = $result->fetch(PDO::FETCH_OBJ);
      }
      while (count($array_of_cards)<10) {
        $array_card=  array('card' => 0, 'value'=>0);
        array_push($array_of_cards,$array_card);
      }
      return $array_of_cards;
    } catch (PDOException $e) {
      echo "Did not manage to get hand of player";
    }
    return null;
  }

  public static function deleteCardFromHand($index_of_game,$index_of_card){
    //on insère la carte dans la BDD
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'DELETE FROM gere WHERE NUMERO_CARTE= '.$index_of_card.' AND ID_PARTIE='.$index_of_game.';';
      // $result = $dbPDO -> prepare('INSERT INTO gere VALUES (:card_id,:game_id,:player_id)');
      // $result->bindParam(':game_id',$index_of_game);
      // $result->bindParam(':card_id',$index_of_card);
      // $result->bindParam(':player_id',$index_of_player);
      $result = $dbPDO->query($query);
      $result->execute();
      return new Hand;
    } catch (PDOException $e) {
      echo "Did not manage to delete the card in gere";
    }
    return null;
  }

  public static function areHandsEmpty($index_of_game){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'SELECT COUNT(NUMERO_CARTE) AS LONGUEUR FROM gere WHERE ID_PARTIE='.$index_of_game.' GROUP BY ID_JOUEUR';
      $result = $dbPDO->query($query);
      $data = $result->fetch(PDO::FETCH_OBJ);
      $array_of_length=array();
      while (!empty($data)) {
        if($data->LONGUEUR != 0){
          return false;
        }
        $data = $result->fetch(PDO::FETCH_OBJ);
      }
      return true;
    } catch (PDOException $e) {
      echo "Didn't manage go get length of hands !";
    }
    return null;
  }

  public static function whoOwnsTheCard($index_of_game,$index_of_card){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'SELECT ID_JOUEUR FROM gere WHERE NUMERO_CARTE='.$index_of_card.' AND ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($query);
      $data=$result->fetch(PDO::FETCH_OBJ);
      return $data->ID_JOUEUR;
    } catch (PDOException $e) {
      echo "Failed to know who owns the card!";
    }
    return null;
  }

  public static function deleteWholeHand($index_of_game){
    try{
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $query = 'DELETE FROM gere WHERE ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($query);
      return new Hand;
    } catch (PDOException $e){
      echo "Couldn't do the cleanup on the hand !";
    }
    return null;
  }

}

 ?>
