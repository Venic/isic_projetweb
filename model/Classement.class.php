<?php
/**
 *
 */
class Classement extends MyObject
{
	private $rang;
	private $pseudo;
	private $partie_jouee;
	private $partie_gagnee;
	private $score_moyen;

    function __construct(){

    }

	public function getRang(){
		return $this->rang;
	}
	public function getPseudo(){
		return $this->pseudo;
	}
	public function getJoue(){
		return $this->partie_jouee;
	}
	public function getGagne(){
		return $this->partie_gagnee;
	}
	public function getMoyen(){
		return $this->score_moyen;
	}
//  public static function addSqlQuery()




	public static function importation($order){
		//Return records from database. Order by chosen parameter.
		if($order==''){
			$order = 'pseudo';
			$type = 'ASC';
		}
		else{
			if($order=='score_moyen' || $order=='pseudo'){
				$type = 'ASC';
			}
			else{
				$type = 'DESC';
			}
		}

		$dbPDO = DatabasePDO::getUniqueDataBase();
		$dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$request = 'SELECT pseudo, partie_jouee, partie_gagnee, score_moyen
			FROM compte
			WHERE score_moyen != 666 ORDER BY '.$order.' '.$type.' ;';
		//echo $request;
		try {
			//Execute the request.
			$result = $dbPDO->prepare($request);
			$result->execute();
			$data = $result->fetchAll(PDO::FETCH_CLASS, 'Classement');
		}catch (PDOException $error){
			echo '456 Failed to connect to MySQL.';
		}
		//On créer le classement corespondant à la requete en cours.
		$compteur = 1;
		foreach($data as $d){
			$d->rang = $compteur;
			$compteur++;
		}
		if($compteur == 1){return NULL;}
		return $data;

	}

	public static function recherche($order,$start,$end){
		//Return a slice of the array returned by the resquest.
		$data=Classement::importation($order);
		if($start<$end && $start<count($data)){
			return array_slice($data,$start,$end+1);
		}
		return array();


	}

	public static function recherche_pseudo($args){
		//Return the record about the required account.
		$dbPDO = DatabasePDO::getUniqueDataBase();
		$dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$request = 'SELECT 1 as rang, pseudo, partie_jouee, partie_gagnee, score_moyen
			FROM compte
			WHERE pseudo="'.$args.'";';
		try {
			$result = $dbPDO->query($request);
			$result->setFetchMode(PDO::FETCH_CLASS, 'Classement');
			$data = $result->fetch();
			return $data;
		}catch (PDOException $error){
			echo '456 Failed to connect to MySQL.';
			return NULL;
		}
	}


	//Fonction to html
	public function allToHtml(){
		$this->toHtml(1,1,1,1);
	}

	public function toHtml($un,$deux,$trois,$quatre){
		if($un+$deux+$trois+$quatre==0){die;}
		else{
			echo '
		<tr>';
			if($un==1){echo '
			<td>'; echo $this->getRang(); echo '</td>';}
			echo '
			<td>'; echo $this->getPseudo(); echo '</td>';
			if($deux==1){echo '
			<td>'; echo $this->getJoue(); echo '</td>';}
			if($trois==1){echo '
			<td>'; echo $this->getGagne(); echo '</td>';}
			if($quatre==1){echo '
			<td>'; echo $this->getMoyen(); echo '</td>';}
			echo '
		</tr>';
		}
	}
	private static function toHtmlEntete($un,$deux,$trois,$quatre){
		if($un+$deux+$trois+$quatre==0){die;}
		else{
			echo '
		<tr>';
			if($un==1){echo '
			<th>Classement</th>';}
			echo '
			<th>Pseudo</th>';
			if($deux==1){echo '
			<th>Parties jouées</th>';}
			if($trois==1){echo '
			<th>Parties gagnées</th>';}
			if($quatre==1){echo '
			<th>Score moyen</th>';}
			echo '
		</tr>';
		}
	}


	//fonction pour le render total.
	public static function classement_toHtml($records, $un,$deux,$trois,$quatre){
		//Ecrit du code Html, le tableau du classement demandé en paramètre.
		Classement::toHtmlEntete($un,$deux,$trois,$quatre);
		foreach($records as $record){
			$record->toHtml($un,$deux,$trois,$quatre);
		}
	}

	public static function recherche_toHtml($records){
		//Ecrit du code Html, les stats du compte entré en paramètre.
		Classement::toHtmlEntete(0,1,1,1);
		$records->toHtml(0,1,1,1);
	}




}
 ?>
