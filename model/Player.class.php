<?php
/**
* List of all functions used in this class
* Name of functions                             Type of element returned        Purpose of the function
* createPlayer                                  Player                          creates a Player in the Database
* getAllCreatedGamesInWhichPlayerIsInvolved     array                           returns an array that contains all the games in which the player is involved
* isPlayerAlreadyOnGame                         boolean                         tries to know if player is already participating in a game
* changePlayerState                             Player                          sets if the player is ready or not
* isPlayerAnHost                                boolean                         tries to know if player is host of the game or not
* getNumberOfPlayersReady                       int / null                      count the number of players for a game that claim to be ready
* isPlayerReady                                 int                             looks if the player is said to be ready for a particular game
* switchHost                                    Player / null                   switch the host of a game if the host decides to leave it.
* getPlayersOnAGame                             array                           looks for all the players of a game and whether they're ready or not.
* getPlayerId                                   int / null                      get the player's id for a particular game.
* getPlayersScore                               array / null                    get the scores of all players in a game.
* getScoreOfAPlayer                             int / null                      get the score of the player
* updatePlayerScore                             Player / null                   update the score of a player
* deleteAllPlayersFromAGame                     Player / null                   deletes all players from a game
* deleteASinglePlayerFromAGame                  Player / null                   delete a player from a game, provieded as a parameter.
*/
class Player extends MyObject
{
  protected $id_player;

  function __construct()
  {
    # code...
  }

  public static function createPlayer($login,$id_partie,$is_host,$is_guest){
    try{
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $count = 'SELECT MAX(ID_JOUEUR) as compteur FROM joueur';
      $result = $dbPDO->query($count);
      $data = $result->fetch(PDO::FETCH_OBJ);
      $id_player = ($data->compteur)+1;
      $request = 'INSERT INTO joueur (ID_JOUEUR,ID_PARTIE, PSEUDO, EN_JEU, HOST, INVITATION,SCORE) VALUES ('.$id_player.','.$id_partie.',"'.$login.'",0,'.$is_host.','.$is_guest.',0);';
	  $result = $dbPDO->query($request);
      //  $result->execute();
      return new Player;
    } catch (PDOException $e){
      echo "Erreur à la création du joueur";
    }
    return null;
  }

  public static function getAllCreatedGamesInWhichPlayerIsInvolved($pseudo){
    try {
      $gamesInvolved = array();
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT partie.ID_PARTIE, partie.NOM_PARTIE, partie.PUBLIQUE, partie.EN_COURS, joueur.EN_JEU, joueur.HOST, joueur.INVITATION FROM joueur,partie WHERE joueur.ID_PARTIE=partie.ID_PARTIE AND joueur.PSEUDO="'.$pseudo.'" ORDER BY joueur.HOST DESC;';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      while (!empty($data)) {
        $infosOnGame = array('game_id'=>$data->ID_PARTIE,'game_name'=>$data->NOM_PARTIE, 'public_private'=>$data->PUBLIQUE, 'on_going'=>$data->EN_COURS, 'is_ready'=>$data->EN_JEU, 'is_host'=>$data->HOST, 'is_guest'=>$data->INVITATION);
        array_push($gamesInvolved,$infosOnGame);
        $data = $result->fetch(PDO::FETCH_OBJ);
      }
      return $gamesInvolved;
    } catch (PDOException $e) {
      echo "Couldn't get the games in which the player was involved";
    }
    return null;
  }

  public static function deleteThePlayerFromAllGamesHeIsInvolved($pseudo){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $delete = 'DELETE FROM joueur WHERE PSEUDO="'.$pseudo.'";';
      $result = $dbPDO->query($delete);
      return true;
    } catch (PDOException $e) {
      echo "Couldn't delete the player in all the games he is involved!";
    }
    return null;
  }

  public static function isPlayerAlreadyOnGame($login){
    //permet de vérifier que le joueur est sur une partie au plus.
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT COUNT(*) AS compteur FROM joueur WHERE PSEUDO="'.$login.'";';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      if ($data->compteur >=1) {
        return true;
      }
    } catch (PDOException $e) {
      echo "Error when checking if player was already in database.";
      return true;
    }
    return false;
  }

  public static function changePlayerState($login,$index_of_game,$new_value){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $update ='UPDATE joueur SET EN_JEU='.$new_value.' WHERE ID_PARTIE='.$index_of_game.' AND PSEUDO="'.$login.'";';
      $result = $dbPDO->query($update);
      $result->execute();
      return 1;
    } catch (PDOException $e) {
      echo "Error when trying to update player's availability.";
    }
    return null;
  }

  public static function isPlayerAnHost($login,$index_of_game){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT HOST FROM joueur WHERE PSEUDO="'.$login.'" AND ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      if ($data->HOST ==1) {
        return true;
      }
      return false;
    } catch (PDOException $e) {
      echo "Couldn't know if player was an host or not.";
    }
    return null;
  }

  public static function getNumberOfPlayersReady($index_of_game){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT COUNT(EN_JEU) AS COMPTEUR FROM joueur WHERE ID_PARTIE='.$index_of_game.' AND EN_JEU=1;';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      if (!empty($data)) {
        return $data->COMPTEUR;
      }
    } catch (PDOException $e) {
      echo "Couldn't get the number of players that are still considered to be playing.";
    }
    return null;
  }

  public static function isPlayerReady($login,$index_of_game){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT EN_JEU FROM joueur WHERE PSEUDO="'.$login.'" AND ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      if (!empty($data)) {
        return $data->EN_JEU;
      }
    } catch (PDOException $e) {
      echo "Error when checking if player was ready";
    }
    return null;
  }

  public static function switchHost($index_of_game,$old_host,$new_host){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request='UPDATE joueur SET HOST=0 WHERE ID_JOUEUR='.$old_host.'; UPDATE joueur SET HOST=1 WHERE ID_JOUEUR='.$new_host.';';
      $result = $dbPDO->query($request);
      return new Player;
    } catch (PDOException $e) {
      echo "Couldn't switch hosts!";
    }
    return null;
  }

  public static function getPlayersOnAGame($id_partie){
    $array_of_players = array();
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT PSEUDO, EN_JEU FROM joueur WHERE ID_PARTIE='.$id_partie.';';
      $result = $dbPDO->query($request);
      $result->execute();
      $data = $result->fetch(PDO::FETCH_OBJ);
      while (!empty($data)) {
        $array_loop = array('login' => $data->PSEUDO, 'ready'=> $data->EN_JEU);
        array_push($array_of_players,$array_loop);
        $data = $result->fetch(PDO::FETCH_OBJ);
      }
      return $array_of_players;
    } catch (PDOException $e) {
      echo "Failed to get players positioned on the game";
    }
    return null;
  }

  public static function getPlayerId($index_of_game,$login){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT ID_JOUEUR FROM joueur WHERE PSEUDO="'.$login.'" AND ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      if (!empty($data)) {
        return $data->ID_JOUEUR;
      }
    } catch (PDOException $e) {
      echo "Erreur à la récupération de l'identifiant";
    }
    return null;
  }

  public static function getPlayersScore($id_partie){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT PSEUDO, SCORE FROM joueur WHERE ID_PARTIE='.$id_partie.' ORDER BY SCORE ASC;';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      $array_of_scores = array();
      while (!empty($data)) {
        $score_of_player = array('pseudo' => $data->PSEUDO, 'score'=>$data->SCORE );
        array_push($array_of_scores,$score_of_player);
        $data = $result->fetch(PDO::FETCH_OBJ);
      }
      return $array_of_scores;
    } catch (PDOException $e) {
      echo "Failed to get players' scores.";
    }
    return null;
  }

  public static function getScoreOfAPlayer($id_partie,$id_player){
    try{
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'SELECT SCORE FROM joueur WHERE ID_PARTIE='.$id_partie.' AND ID_JOUEUR='.$id_player.';';
      $result = $dbPDO->query($request);
      $data = $result->fetch(PDO::FETCH_OBJ);
      if (!empty($data)) {
        return $data->SCORE;
      }
    } catch (PDOException $e){
      echo "Couldn't fetch the score of the player ! ";
    }
    return null;
  }

  public static function updatePlayerScore($id_partie,$id_player,$points_to_add){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request='UPDATE joueur SET SCORE=SCORE+'.$points_to_add.' WHERE ID_PARTIE='.$id_partie.' AND ID_JOUEUR='.$id_player.';';
      $result = $dbPDO->query($request);
      return new Player;
    } catch (Exception $e) {
      echo "Failed to update the player's score.";
    }
    return null;
  }

  public static function deleteAllPlayersFromAGame($index_of_game){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'DELETE FROM joueur WHERE ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($request);
      return new Player;
    } catch (PDOException $e) {
      echo "Could not delete informations about a game!";
    }
    return null;
  }

  public static function deleteASinglePlayerFromAGame($index_of_player,$index_of_game){
    try {
      $dbPDO = DatabasePDO::getUniqueDataBase();
      $dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $request = 'DELETE FROM joueur WHERE ID_JOUEUR='.$index_of_player.' AND ID_PARTIE='.$index_of_game.';';
      $result = $dbPDO->query($request);
      return new Player;
    } catch (PDOException $e) {
      echo "Didn't manage to delete the player from the table players!";
    }
    return null;
  }


  public static function getAllPlayersScoreInHTML($array){
  //  $allocated_space = 12/count($array);
    $finalString = '';
    for ($i=0; $i < count($array) ; $i++) {
      $scoreOfPlayer = $array[$i];
      $finalString = $finalString.'<div class="col-sm-1">';
      if ($scoreOfPlayer['score']<2) {
        $finalString = $finalString.'<span class="player-name-board">'.$scoreOfPlayer['pseudo'].'</span><br><span class="player-score-board">'.$scoreOfPlayer['score'].' pt</span>';
      } else{
        $finalString = $finalString.'<span class="player-name-board">'.$scoreOfPlayer['pseudo'].'</span><br><span class="player-score-board">'.$scoreOfPlayer['score'].' pts</span>';
      }
      $finalString = $finalString.'</div>';
    }
    return $finalString;
  }
}
?>
