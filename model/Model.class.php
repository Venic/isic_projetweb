<?php
/*
* Root class of all my classes
*/
class Model extends MyObject {
	//protected static $dataBase;
	
	function __construct($argument)
	{
			//code
    }
  
	protected static function getCurrentDataBase()
	{
		return DatabasePDO::getUniqueDataBase();
		//Renvoie l'unique instance de PDO vers la base.
	}
	
	protected static function query($sql)
	{
		$st = static::getCurrentDataBase()->query($sql) or die("sql query error ! request : " . $sql);
		$st->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, get_called_class());
		return $st;
	}

}
?>