<?php
//Création de compte
User::addSqlQuery('CREATE_ACCOUNT','INSERT INTO compte VALUES(:pseudo,:password,0,0,0,0,NULL)');




//Parties publiques non commencées : informations à afficher : nom du joueur, nom de la partie et nombre de joueurs
//On fait en deux temps : un premier pour récupérer le nom de la partie et la somme des joueurs en attente:
User::addSqlQuery('GET_AVAILABLE_PUBLIC_GAMES','SELECT partie.NOM_PARTIE, partie.EN_COURS, partie.PUBLIQUE, COUNT(joueur.PSEUDO) AS somme FROM partie JOIN joueur ON partie.ID_PARTIE=joueur.ID_PARTIE WHERE (partie.PUBLIQUE=1 AND partie.EN_COURS=0 /*AND somme < 10*/ ) GROUP BY joueur.ID_PARTIE ORDER BY joueur.ID_PARTIE ASC');
//Deuxième requête pour récupérer le nom du créateur
User::addSqlQuery('GET_AVAILABLE_PUBLIC_GAMES_HOST','SELECT joueur.PSEUDO FROM joueur JOIN partie ON joueur.ID_PARTIE=partie.ID_PARTIE WHERE partie.PUBLIQUE=1 AND joueur.HOST=1 AND partie.EN_COURS=0 ORDER BY joueur.ID_PARTIE ASC');
//Créer une partie qui par défaut n'est pas en cours
User::addSqlQuery('CREATE_GAME','INSERT INTO partie (NOM_PARTIE,PUBLIQUE,EN_COURS) VALUES(:gamename,:publique,0)');
//NOUVELLE UTILISATION du champ EN_JEU, il permet aux joueurs qui se considèrent comme prêts de se signaler. Dès qu'on est à 1 sur tous les joueurs d'une partie, on lance le jeu.
//1 : joueur prêt à jouer, partie en cours
//0 : joueur pas prêt à jouer, partie pas en cours (par défaut avant démarrage et à la fin de la partie)
User::addSqlQuery('BE_READY','UPDATE joueur SET EN_JEU=:actifPassif WHERE ID_JOUEUR=:identifiantjoueur');
//Dès qu'on atteint 10 joueurs ou que tous les joueurs sont réglés sur 1 et que le host est à 1, création de la partie.
User::addSqlQuery('LAUNCH_GAME','UPDATE partie SET EN_COURS=:actifPassif WHERE partie.ID_PARTIE=:identifiantpartie');
//Création des mains
User::addSqlQuery('CREATE_HAND','INSERT INTO gere (NUMERO_CARTE,ID_PARTIE,ID_JOUEUR) VALUES(:cardnumber,:identifiantpartie,:identifiantjoueur)');
//Création du plateau (à faire 4 fois au lancement du jeu)
User::addSqlQuery('CREATE_BOARD','INSERT INTO possede (NUMERO_CARTE, ID_PARTIE, NUMERO_RANGEE) VALUES(:cardnumber,:identifiantpartie,:rownumber)');
//Ajout d'une carte sur une rangée : comptage du nombre de cartes dans la rangée, ajout dans la table possède, retrait dans la table gère
User::addSqlQuery('COUNT_CARDS_ON_ROW','SELECT COUNT(NUMERO_CARTE) FROM possede WHERE ID_PARTIE=:identifiantpartie AND NUMERO_RANGEE=:rownumber)');
//Si la rangée est pleine, on la vide : transfert de possede à RECOIT. 1. On récupère l'identifiant, 2. on transfère, 3. on efface
User::addSqlQuery('GET_CARD_ID_ON_ROW','SELECT NUMERO_CARTE FROM possede WHERE ID_PARTIE=:identifiantpartie AND NUMERO_RANGEE=:rownumber');
User::addSqlQuery('INSERT_CARD_ON_STACK','INSERT INTO recoit (NUMERO_CARTE, ID_PARTIE, ID_JOUEUR) VALUES(:cardnumber,:identifiantpartie,:identifiantjoueur)');
User::addSqlQuery('DELETE_CARD_FROM_BOARD','DELETE FROM possede WHERE ID_PARTIE=:identifiantpartie AND NUMERO_RANGEE=:rownumber');
//Insertion de la carte
User::addSqlQuery('ADD_CARD_TO_BOARD','INSERT INTO possede (NUMERO_CARTE,ID_PARTIE,NUMERO_RANGEE) VALUES(:cardnumber,:identifiantpartie,:rownumber)');
User::addSqlQuery('DELETE_CARD_FROM_HAND','DELETE FROM gere WHERE NUMERO_CARTE=:cardnumber AND ID_PARTIE=:identifiantpartie ');

//Affichage des scores. Tenter d'afficher aussi les 0 !!!
User::addSqlQuery('GET_GAME_SCORES','SELECT recoit.ID_JOUEUR, SUM(carte.VALEUR) as somme FROM recoit JOIN carte ON recoit.NUMERO_CARTE = carte.NUMERO_CARTE WHERE recoit.ID_PARTIE=:identifiantpartie GROUP BY recoit.ID_JOUEUR ORDER BY somme ASC');

//Fin de partie : détermination du meilleur joueur, puis ajout d'une unité. A modifier pour permmettre les exco de gagner tous. En plus c fô. La perfection n'est pas prise en compte.
User::addSqlQuery('GET_MINIMAL_SCORE','SELECT recoit.ID_JOUEUR, SUM(carte.VALEUR) as somme FROM recoit JOIN carte ON recoit.NUMERO_CARTE = carte.NUMERO_CARTE WHERE recoit.ID_PARTIE=:identifiantpartie GROUP BY recoit.ID_JOUEUR ORDER BY somme ASC LIMIT 0,1');
User::addSqlQuery('UPDATE_AVERAGE_SCORE','UPDATE compte SET SCORE_MOYEN = (SCORE_MOYEN*PARTIE_JOUEE+:playerscore)/(PARTIE_JOUEE+1) WHERE PSEUDO=:accountid');
User::addSqlQuery('UPDATE_WINNER_ACCOUNT','UPDATE compte SET PARTIE_GAGNEE = PARTIE_GAGNEE + 1 WHERE PSEUDO =:accountid ');
User::addSqlQuery('UPDATE_COUNTERS_ACCOUNT','UPDATE compte SET PARTIE_JOUEE = PARTIE_JOUEE + 1 WHERE PSEUDO =:accountid ');
//La partie n'est plus considérée comme en cours et les joueurs comme plus en jeu, on nettoie le board, le tas, les mains, si l'host quitte la partie, la partie est détruite.
User::addSqlQuery('CLEAN_BOARD','DELETE FROM possede WHERE ID_PARTIE=:identifiantpartie');
User::addSqlQuery('CLEAN_STACK','DELETE FROM recoit WHERE ID_PARTIE=:identifiantpartie');
User::addSqlQuery('CLEAN_HAND','DELETE FROM gere WHERE ID_PARTIE=:identifiantpartie');

//Sur la page d'accueil de l'utilisateur
//User:addSqlQuery('DISPLAY_STATS','SELECT PARTIE_JOUEE,PARTIE_GAGNEE,SCORE_MOYEN FROM compte WHERE PSEUDO=:pseudo')

//Nombre de partie gagnées
?>
