/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de cr�ation :  15/02/2017 11:29:29                      */
/*==============================================================*/


drop table if exists CARTE;

/*drop index HISTORIQUE on COMPTE;*/

drop table if exists COMPTE;

drop table if exists GERE;

drop table if exists JOUEUR;

/*drop index LIBRE on PARTIE;*/

drop table if exists PARTIE;

drop table if exists POSSEDE;

drop table if exists RECOIT;

/*==============================================================*/
/* Table : CARTE                                                */
/*==============================================================*/
create table CARTE
(
   NUMERO_CARTE         int not null,
   VALEUR               int,
   primary key (NUMERO_CARTE)
);

/*==============================================================*/
/* Table : COMPTE                                               */
/*==============================================================*/
create table COMPTE
(
   PSEUDO               varchar(15),
   MOT_DE_PASSE         varchar(15),
   PARTIE_JOUEE         int,
   PARTIE_GAGNEE        int,
   SCORE_MOYEN          decimal(5,2),
   LEAVER_BUSTER        int,
   BLOQUE               datetime,
   primary key (PSEUDO)
);

/*==============================================================*/
/* Index : HISTORIQUE                                           */
/*==============================================================*/
create index HISTORIQUE on COMPTE
(
   PARTIE_JOUEE,
   PARTIE_GAGNEE
);

/*==============================================================*/
/* Table : GERE                                                 */
/*==============================================================*/
create table GERE
(
   NUMERO_CARTE         int not null,
   ID_PARTIE            int not null,
   ID_JOUEUR            int not null,
   primary key (ID_PARTIE, NUMERO_CARTE, ID_JOUEUR)
);

/*==============================================================*/
/* Table : JOUEUR                                               */
/*==============================================================*/
create table JOUEUR
(
   ID_PARTIE            int not null,
   ID_JOUEUR            int not null,
   PSEUDO               varchar(15),
   EN_JEU               bool,
   HOST                 bool,
   INVITATION           bool,
   primary key (ID_PARTIE, ID_JOUEUR)
);

/*==============================================================*/
/* Table : PARTIE                                               */
/*==============================================================*/
create table PARTIE
(
   ID_PARTIE            int not null,
   NOM_PARTIE           text,
   PUBLIQUE             bool,
   EN_COURS             bool,
   primary key (ID_PARTIE)
);

/*==============================================================*/
/* Index : LIBRE                                                */
/*==============================================================*/
create index LIBRE on PARTIE
(
   PUBLIQUE,
   EN_COURS
);

/*==============================================================*/
/* Table : POSSEDE                                              */
/*==============================================================*/
create table POSSEDE
(
   NUMERO_CARTE         int not null,
   ID_PARTIE            int not null,
   NUMERO_RANGEE        int,
   primary key (NUMERO_CARTE, ID_PARTIE)
);

/*==============================================================*/
/* Table : RECOIT                                               */
/*==============================================================*/
create table RECOIT
(
   NUMERO_CARTE         int not null,
   ID_PARTIE            int not null,
   ID_JOUEUR            int not null,
   primary key (ID_PARTIE, NUMERO_CARTE, ID_JOUEUR)
);

alter table GERE add constraint FK_GERE foreign key (NUMERO_CARTE)
      references CARTE (NUMERO_CARTE) on delete restrict on update cascade;

alter table GERE add constraint FK_GERE2 foreign key (ID_PARTIE, ID_JOUEUR)
      references JOUEUR (ID_PARTIE, ID_JOUEUR) on delete cascade on update cascade;

alter table JOUEUR add constraint FK_FAIT_JOUER foreign key (ID_PARTIE)
      references PARTIE (ID_PARTIE) on delete cascade on update cascade;

alter table JOUEUR add constraint FK_SONT foreign key (PSEUDO)
      references COMPTE (PSEUDO) on delete restrict on update restrict;

alter table POSSEDE add constraint FK_POSSEDE foreign key (NUMERO_CARTE)
      references CARTE (NUMERO_CARTE) on delete restrict on update cascade;

alter table POSSEDE add constraint FK_POSSEDE2 foreign key (ID_PARTIE)
      references PARTIE (ID_PARTIE) on delete cascade on update cascade;

alter table RECOIT add constraint FK_RECOIT foreign key (NUMERO_CARTE)
      references CARTE (NUMERO_CARTE) on delete restrict on update cascade;

alter table RECOIT add constraint FK_RECOIT2 foreign key (ID_PARTIE, ID_JOUEUR)
      references JOUEUR (ID_PARTIE, ID_JOUEUR) on delete cascade on update cascade;
